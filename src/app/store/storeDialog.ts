import { Component, OnInit, Inject } from "@angular/core";
import { Subject } from "../card-detail";
import { HomeServiceService } from "../home/home-service.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { StoreService } from "../service/store.service";
import { UserService } from "../service/user.service";

// ///////////////////// RESTORE.CM
@Component({
  templateUrl: "restoreDialog.html",
  styleUrls: ["./store.component.scss"]
})
export class RestoreDialog {
  subjects: Subject[];
  constructor(
    public dialogRef: MatDialogRef<RestoreDialog>,
    private homeInService: HomeServiceService,
    private storeService: StoreService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  onDelete() {
    this.storeService.restoreClass(this.homeInService.getSubject().idpk);
    this.dialogRef.close("delete");
    ///lostClassroom
  }
}
// ///////////////////// LOST SURE.CM
@Component({
  templateUrl: "lostDialog.html",
  styleUrls: ["./store.component.scss"]
})
export class LostDialog {
  subjects: Subject[];
  constructor(
    public dialogRef: MatDialogRef<LostDialog>,
    private homeInService: HomeServiceService,
    private storeService: StoreService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  onDelete() {
    this.storeService.lostClass(this.homeInService.getSubject().idpk);
    this.dialogRef.close("delete");
  }
}
