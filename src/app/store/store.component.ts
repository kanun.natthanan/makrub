import { Component, OnInit } from "@angular/core";
import { Subject } from "../card-detail";
import { StoreService } from "../service/store.service";
import { UserService } from "../service/user.service";
import { Router } from "@angular/router";
import { HomeServiceService } from "../home/home-service.service";
import { MatDialog } from "@angular/material/dialog";
import { RestoreDialog, LostDialog } from "./storeDialog";
import { ProfileService } from "../service/profile.service";

@Component({
  selector: "app-store",
  templateUrl: "./store.component.html",
  styleUrls: ["./store.component.scss"],
})
export class StoreComponent implements OnInit {
  subjects: Subject[];
  subjects_new: Subject[];
  selectSubject: Subject;
  imageUrl = "";

  constructor(
    private router: Router,
    private storeService: StoreService,
    private userService: UserService,
    private homeInService: HomeServiceService,
    public dialog: MatDialog,
    private profile: ProfileService
  ) {}

  ngOnInit() {
    this.subjects = [];
    this.profile.getImage(this.userService.getUser()).subscribe(
      (res) => {
        if (res["img"].length == 0) {
          this.imageUrl = "../../assets/img/people.png";
        } else {
          this.imageUrl = "http://thebeach.buu.in.th:4011/images/" + res["img"];
        }
      },
      (err) => {
        console.log("IMG ERROR");
      }
    );
    this.reload("start");
  }
  onLogout() {
    this.router.navigate(["/"]);
  }
  onProfile() {
    this.router.navigate(["/profile"]);
  }
  onSelect(sub: Subject): void {
    this.selectSubject = sub;
    // console.log(sub)
    this.homeInService.setSubject(this.selectSubject);

    if (this.homeInService.getAction() === "restore") {
      const dialogRef = this.dialog.open(RestoreDialog, {
        panelClass: "custom-dialog-container",
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result == "delete") {
          if (this.subjects.length == 1) {
            this.subjects.length = 0;
          } else {
            this.reload("delete");
          }
        }
      });
    } else if (this.homeInService.getAction() === "delete100") {
      const dialogRef = this.dialog.open(LostDialog, {
        panelClass: "custom-dialog-container",
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result === "delete") {
          if (this.subjects.length == 1) {
            this.subjects.length = 0;
          } else {
            this.reload("delete");
          }
        }
      });
    } else {
      // console.log(sub);
    }
    this.homeInService.setAction(undefined);
  }
  reload(action) {
    this.storeService.getStroe(this.userService.getUser()).subscribe((res) => {
      for (var x in res) {
        for (var y in res[x].member) {
          if (this.userService.getUser() === res[x].member[y].username) {
            status = res[x].member[y].status;
          }
        }
        if (action === "start") {
          if (status === "เจ้าของห้อง") {
            this.subjects.push({
              group: res[x].group,
              status: status,
              pic_id: res[x].pic_id,
              subject_name: res[x].name,
              idpk: res[x]._id,
              detail: res[x].detail,
              code: res[x].code,
              sort: res[x].numberClass,
              store: res[x].status,
            });
            // console.log(res);
          }
        } else if (action === "delete") {
          for (var x in this.subjects) {
            if (
              this.subjects[x].idpk === this.homeInService.getSubject().idpk
            ) {
              try {
                this.subjects.splice(parseInt(x), 1);
              } catch (error) {
                this.subjects.pop();
              }
              break;
            }
          }
          break;
        }
      }
    });
    this.subjects_new = this.homeInService.getSubjects();
  }
  onGoTo(sub: Subject): void {
    this.selectSubject = sub;
    // console.log(this.selectSubject);
    this.homeInService.setSubject(this.selectSubject);
    if (this.homeInService.getAction() === undefined) {
      this.homeInService.toChaeckList(this.selectSubject);
      // console.log(this.selectSubject.status);
      if (this.selectSubject.status === "เจ้าของห้อง") {
        this.router.navigate(["/listCheck"]);
      } else {
        this.router.navigate(["/classMember"]);
      }
    }
    this.homeInService.setAction(undefined);
  }
}
