import { Injectable } from "@angular/core";
import { environment } from "../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ListData } from "../class/listcheck/listcheck.component";

@Injectable({
  providedIn: "root"
})
export class ListcheckService {
  noAuthHeader = { headers: new HttpHeaders({ NoAuth: "True" }) };
  listData: ListData[];

  constructor(private http: HttpClient) {}
  getListCheck(idClass) {
    var list = {
      idClass: idClass
    };
    return this.http.post(
      environment.apiBaseUrl + "/getListClass",
      list,
      this.noAuthHeader
    );
  }
  addListCheck(list) {
    var listClass = {
      idClass: list.idpk,
      type: list.type,
      hour: list.hour + "",
      name: list.name,
      late: list.late + "",
      start: list.start,
      date: list.date,
    };
    // console.log(list)
    this.http
      .post(environment.apiBaseUrl + "/addList", listClass, this.noAuthHeader)
      .subscribe(
        res => {
          // console.log(res);
        },
        err => {
          console.log("ERROR INSERT");
        }
      );
  }
  updateListCheck(list) {
    var lists = {
      idList: list.idpk,
      name: list.name,
      type: list.type,
      hour: list.hour + "",
      date: list.date,
      late: list.late + "",
      start: list.start
    };
    // console.log(lists);
    this.http
      .post(environment.apiBaseUrl + "/editList", lists, this.noAuthHeader)
      .subscribe(
        res => {
          // console.log(res);
        },
        err => {
          console.log("ERROR UPDATE");
        }
      );
  }
  deleteListCheck(idList) {
    var list = {
      idList: idList
    };
    this.http
      .post(environment.apiBaseUrl + "/deleteList", list, this.noAuthHeader)
      .subscribe(
        res => {
          // console.log(res);
        },
        err => {
          console.log("ERROR DELETE");
        }
      );
  }
  updateManuakCheck(idList, manualCheck) {
    var check = {
      idList: idList,
      member: manualCheck
    };
    // console.log(check);
    this.http
      .post(
        environment.apiBaseUrl + "/updateCheckList",
        check,
        this.noAuthHeader
      )
      .subscribe(
        res => {
          // console.log(res);
        },
        err => {
          console.log("ERROR UPDATE");
        }
      );
  }
  updateBarcodeCheck(check) {
    return this.http.post(
      environment.apiBaseUrl + "/updateCheckList",
      check,
      this.noAuthHeader
    );
  }
  isMember(idList, username) {
    var list = {
      idList: idList
    };
    return this.http.post(
      environment.apiBaseUrl + "/resultOneList",
      list,
      this.noAuthHeader
    );
  }
  getImg(username) {
    var user = {
      username: username
    };
    return this.http.post(
      environment.apiBaseUrl + "/getuser",
      user,
      this.noAuthHeader
    );
  }
  getDetailList(idList) {
    var list = {
      idList: idList
    };

    return this.http.post(
      environment.apiBaseUrl + "/getOneList",
      list,
      this.noAuthHeader
    );
  }
  getMember(rfid){
    var member = {
      rfid: rfid
    }
    return this.http.post(
      environment.apiBaseUrl + "/getProfileByRFID",
      member,
      this.noAuthHeader
    );
  }
}
