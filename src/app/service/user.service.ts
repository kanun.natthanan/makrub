import { Injectable } from "@angular/core";
import { environment } from "../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class UserService {
  noAuthHeader = { headers: new HttpHeaders({ NoAuth: "True" }) };

  constructor(private http: HttpClient) {}

  login(authCredentials) {
    // console.log(authCredentials);
    // console.log(environment.apiBaseUrl);
    return this.http.post(
      environment.apiBaseUrl + "/login",
      authCredentials,
      this.noAuthHeader
    );
  }

  setToken(token: string) {
    localStorage.setItem("token", token);
  }

  getToken() {
    return localStorage.getItem("token");
  }

  deleteToken() {
    localStorage.removeItem("token");
  }
  setUser(user: string) {
    localStorage.setItem("user", user);
  }

  getUser() {
    return localStorage.getItem("user");
  }

  deleteUser() {
    localStorage.removeItem("user");
  }

  getUserPayload() {
    var token = this.getToken();
    if (token) {
      var userPayload = atob(token.split(".")[1]);
      return JSON.parse(userPayload);
    } else return null;
  }

  isLoggedIn() {
    var userPayload = this.getUserPayload();
    if (userPayload) return userPayload.exp > Date.now() / 1000;
    else return false;
  }
}
