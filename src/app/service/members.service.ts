import { Injectable } from "@angular/core";
import { environment } from "../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";



@Injectable({
  providedIn: "root"
})
export class MembersService {
  noAuthHeader = { headers: new HttpHeaders({ NoAuth: "True" }) };
  contactsList:string[] = [];

  constructor(private http: HttpClient) {}
  getMembers(idClass) {
    var classRoom = {
      idClass: idClass
    };
    return this.http.post(
      environment.apiBaseUrl + "/getMemberInClass",
      classRoom,
      this.noAuthHeader
    );
  }
  deleteMember(idClass, username) {
    var member = {
      idClass: idClass,
      username: username
    };
    // console.log(member);
    this.http
      .post(environment.apiBaseUrl + "/deleteMember", member, this.noAuthHeader)
      .subscribe(
        res => {
          // console.log(res);
        },
        err => {
          console.log("ERROR DELETE");
        }
      );
  }
  getMemberManual(idList) {
    var list = {
      idList: idList
    };
    return this.http.post(
      environment.apiBaseUrl + "/resultOneList",
      list,
      this.noAuthHeader
    );
  }
  addMemberExcel(members, idClass) {
    let data = {
      idClass: idClass,
      members: members
    };
    // console.log(data);
    return this.http.post(
      environment.apiBaseUrl + "/uploadMemberByExcel",
      data,
      this.noAuthHeader
    );
  }
  registerCard(username, rfid) {
    var card = {
      username: username,
      rfid: rfid
    };
    this.http
      .post(environment.apiBaseUrl + "/registerRFID", card, this.noAuthHeader)
      .subscribe(
        res => {
          // console.log(res);
        },
        err => {
          console.log("ERROR UPDATE");
        }
      );
  }
  setContactsList(list){
    this.contactsList = list
  }
}
