import { Injectable } from "@angular/core";
import { environment } from "../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";

/////////////
import { Subject } from "../card-detail";
import { UserService } from "./user.service";

@Injectable({
  providedIn: "root"
})
export class HomeService {
  noAuthHeader = { headers: new HttpHeaders({ NoAuth: "True" }) };
  subjects: Subject[];

  constructor(private http: HttpClient, private userService: UserService) {}

  getHome(usernames) {
    var user = {
      username: usernames
    };
    // console.log(user)
    return this.http.post(
      environment.apiBaseUrl + "/getClassroomUser",
      user,
      this.noAuthHeader
    );
  }

  addClass(subject_for_insert) {
    // console.log(subject_for_insert);
    var model = {
      name: subject_for_insert.subject_name,
      detail: subject_for_insert.detail,
      pic_id: subject_for_insert.pic_id,
      group: subject_for_insert.group,
      username: this.userService.getUser()
    };
    this.http
      .post(environment.apiBaseUrl + "/addClassroom", model, this.noAuthHeader)
      .subscribe(
        res => {
          // console.log(res);
        },
        err => {
          console.log("ERROR INSERT");
        }
      );
    // console.log(this.userService.getUser());
  }

  deleteClass(subject_for_delete) {
    // console.log(subject_for_delete);
    const model = {
      idClass: subject_for_delete
    };
    this.http
      .post(
        environment.apiBaseUrl + "/deleteClassroom",
        model,
        this.noAuthHeader
      )
      .subscribe(
        res => {
          // console.log(res);
        },
        err => {
          console.log("ERROR DELETE");
        }
      );
  }

  editClass(subject_for_edit) {
    // console.log(subject_for_edit);
    var model = {
      id: subject_for_edit.idpk,
      name: subject_for_edit.subject_name,
      detail: subject_for_edit.detail,
      pic_id: subject_for_edit.pic_id,
      group: subject_for_edit.group
    };
    // console.log(model);
    this.http
      .post(environment.apiBaseUrl + "/editClassroom", model, this.noAuthHeader)
      .subscribe(
        res => {
          // console.log(res);
        },
        err => {
          console.log("ERROR EDIT");
        }
      );
  }
  joinClass(user) {
    // console.log(user);
    this.http
      .post(
        environment.apiBaseUrl + "/addMemberByCode",
        user,
        this.noAuthHeader
      )
      .subscribe(res => {
        // console.log(res);
      },
      err => {
        console.log("JOIN ERROR")
      });
  }
}
