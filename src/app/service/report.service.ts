import { Injectable } from '@angular/core';
import { environment } from "../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  noAuthHeader = { headers: new HttpHeaders({ NoAuth: "True" }) };

  constructor(private http: HttpClient, ) { }
  getReportAll(idClass){
    var classRoom = {
      idClass: idClass
    };
    return this.http
      .post(
        environment.apiBaseUrl + "/resultAllListOwner",
        classRoom,
        this.noAuthHeader
      ) 
  }
  getReportList(idList){
    var list = {
      idList: idList
    };
    return this.http
    .post(
      environment.apiBaseUrl + "/resultOneList",
      list,
      this.noAuthHeader
    ) 

  }
  getReportMember(idClass,username){
    var report = {
      idClass: idClass,
      username: username,
    }
    return this.http
      .post(
        environment.apiBaseUrl + "/resultAllListMember",
        report,
        this.noAuthHeader
      )
  }
}
