import { Injectable } from "@angular/core";
import { environment } from "../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { UserData } from "../profile/personal/personal.component";

@Injectable({
  providedIn: "root",
})
export class ProfileService {
  noAuthHeader = { headers: new HttpHeaders({ NoAuth: "True" }) };
  userData: UserData = {
    username: "",
    nameEN: "",
    nameTH: "",
    university: "",
    faculty: "",
    img: "",
  };
  image: string;
  constructor(private http: HttpClient) {}
  getProfile(username) {
    var user = {
      username: username,
    };
    this.http
      .post(environment.apiBaseUrl + "/getuser", user, this.noAuthHeader)
      .subscribe(
        (res) => {
          // console.log(res);
          this.userData.username = res["username"];
          this.userData.nameEN = res["firstnameEng"] + " " + res["lastnameEng"];
          this.userData.nameTH = res["firstnameTh"] + " " + res["lastnameTh"];
          this.userData.faculty = res["faculty"];
          this.userData.university = res["university"];
          this.userData.img = res["img"];
        },
        (err) => {
          // console.log("1124");
        }
      );
    return this.userData;
  }
  getImage(username) {
    var user = {
      username: username,
    };
    return this.http.post(
      environment.apiBaseUrl + "/getuser",
      user,
      this.noAuthHeader
    );
  }
  updateNameTH(username, firstname, lastname) {
    var nameTH = {
      username: username,
      firstnameTh: firstname,
      lastnameTh: lastname,
    };
    this.http
      .post(environment.apiBaseUrl + "/editUser", nameTH, this.noAuthHeader)
      .subscribe(
        (res) => {},
        (err) => {
          // console.log("ERROR UPDATE");
        }
      );
  }
  uploadImg(data) {
    // console.log(data);
    return this.http
      .post(environment.apiBaseUrl + "/uploadImg", data, this.noAuthHeader)
      .subscribe(
        (res) => {
          // console.log(res);
        },
        (err) => {
          console.log("UPLOAD IMG ERROR");
        }
      );
  }
  hasProfile(username) {
    let data = {
      username: username,
    };
    return this.http.post(
      environment.apiBaseUrl + "/getuser",
      data,
      this.noAuthHeader
    );
  }
}
