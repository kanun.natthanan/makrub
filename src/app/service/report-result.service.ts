import { Injectable } from "@angular/core";
import { environment } from "../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ReportResultService {
  noAuthHeader = { headers: new HttpHeaders({ NoAuth: "True" }) };

  constructor(private http: HttpClient) {}

  getReportAll(idClass){
    var classRoom = {
      idClass: idClass
    };
    return this.http
      .post(
        environment.apiBaseUrl + "/getListClass",
        classRoom,
        this.noAuthHeader
      ) 
  }
  
}
