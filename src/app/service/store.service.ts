import { Injectable } from "@angular/core";
import { environment } from "../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class StoreService {
  noAuthHeader = { headers: new HttpHeaders({ NoAuth: "True" }) };

  constructor(private http: HttpClient) {}
  getStroe(usernames) {
    var user = {
      username: usernames
    };
    // console.log(user)
    return this.http.post(
      environment.apiBaseUrl + "/getClassroomFalse",
      user,
      this.noAuthHeader
    );
  }
  restoreClass(subject_for_restore) {
    // console.log(subject_for_restore);
    const model = {
      idClass: subject_for_restore
    };
    this.http
      .post(
        environment.apiBaseUrl + "/recoverClassroom",
        model,
        this.noAuthHeader
      )
      .subscribe(
        res => {
          // console.log(res);
        },
        err => {
          console.log("ERROR RESTORE");
        }
      );
  }
  lostClass(subject_for_delete : String) {
    // console.log(subject_for_delete);
    const model = {
      idClass: subject_for_delete
    };
    this.http
      .post(environment.apiBaseUrl + "/lostClassroom", model, this.noAuthHeader)
      .subscribe(
        res => {
          // console.log(res);
        },
        err => {
          console.log("ERROR DELETE");
        }
      );
  }
}
