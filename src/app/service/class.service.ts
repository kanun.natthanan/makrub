import { Injectable } from "@angular/core";
import { environment } from "../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { UserService } from 'src/app/service/user.service';
export interface  member{
  username: string;
  name: string;
}
export interface list{
  id: string;
  name: string;
}

@Injectable({
  providedIn: "root"
})
export class ClassService {
  noAuthHeader = { headers: new HttpHeaders({ NoAuth: "True" }) };
  reportMember: member = {username: "",name:""}
  titleList: list = {id: "",name:""};

  constructor(private http: HttpClient) {}
  getReportMember(){
    let data = localStorage.getItem(".reportMember");
    return JSON.parse(data)
  }
  getTitleList(){
    let data = localStorage.getItem(".titleList");
    return JSON.parse(data)
  }
  getPathBefore(){
    let data = localStorage.getItem(".pathBefore");
    return JSON.parse(data)
  }
}
