import { Router } from "@angular/router";
import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { ProfileService } from "../service/profile.service";
import { ProfileComponent } from "../profile/profile.component";
import { UserService } from "../service/user.service";
import { ClassService } from "../service/class.service";
import { ReportService } from "../service/report.service";
import { HomeServiceService } from "../home/home-service.service";
import { Subject } from "../card-detail";
export interface Room {
  name: string;
  group: string;
}

export interface Class {
  name: string;
  group: string;
}

export interface UserData {
  id: number;
  list: string;
  type: string;
  hours: number;
  date: string;
  statusCheck: string;
}

export interface SumReport{
  onTime: number;
  late: number;
  absent: number;
}

@Component({
  selector: "app-class-member",
  templateUrl: "./class-member.component.html",
  styleUrls: ["./class-member.component.scss"]
})
export class ClassMemberComponent implements OnInit {
  opened = false;
  links = ["report"];
  menus = ["รายงานการเช็คชื่อ"];
  class: Class = { name: "88625159 Mobile Programing", group: "2" };
  userData: UserData[];
  sumReport:SumReport = {onTime:0,late:0,absent:0};

  imageUrl: string = "";
  private selectSubject;
  private subjects_new;
  displayedColumns: string[] = [
    "id",
    "list",
    "type",
    "hours",
    "date",
    "statusCheck"
  ];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private router: Router,
    private profile: ProfileService,
    private user: UserService,
    public member: ClassService,
    private report: ReportService,
    private home: HomeServiceService
  ) {}

  ngOnInit() {
    this.subjects_new = this.home.getSubjects();
    this.profile.getImage(this.user.getUser()).subscribe(
      (res) => {
        if (res["img"].length == 0) {
          this.imageUrl = "../../assets/img/people.png";
        } else {
          this.imageUrl = "http://thebeach.buu.in.th:4011/images/" + res["img"];
        }
      },
      (err) => {
        console.log("IMG ERROR")
      }
    );
    this.createTable();
    // console.log(this.home.getData())
    this.class.name = this.home.getData().name + " " + this.home.getData().detail;
    this.class.group = this.home.getData().group;
  }
  onProfile() {
    this.router.navigate(["/profile"]);
  }
  onLogout() {
    localStorage.clear();
    this.router.navigate(["/"]);
  }

  createTable() {
    // console.log(this.home.getData().id, this.user.getUser());
    this.report
      .getReportMember(this.home.getData().id, this.user.getUser())
      .subscribe(
        res => {
          var round = 0;
          this.userData = [];
          var status = "";
          // console.log(res);
          for (var x in res) {
            round++;
            if (res[x].status == "onTime") {
              status = "ทันเวลา";
              this.sumReport.onTime = this.sumReport.onTime+1;
            } else if (res[x].status == "late") {
              status = "สาย";
              this.sumReport.late = this.sumReport.late+1;
            } else if (res[x].status == "leave") {
              status = "ลากิจ";
              this.sumReport.absent = this.sumReport.absent+1
            } else if (res[x].status == "sick") {
              status = "ลาป่วย";
              this.sumReport.absent = this.sumReport.absent+1
            } else if (res[x].status == "notSendLeave") {
              status = "ไม่ส่งใบลา";
              this.sumReport.absent = this.sumReport.absent+1
            } else if (res[x].status == "empty") {
              status = "-";
              this.sumReport.absent = this.sumReport.absent+1
            }
            this.userData.push({
              id: round,
              list: res[x].name,
              type: res[x].type,
              hours: res[x].hour,
              date: res[x].date,
              statusCheck: status
            });
          }

          this.getTable();
        },
        err => {
          console.log("ERROR CREATE TABLE");
        }
      );
  }
  getTable() {
    this.dataSource = new MatTableDataSource(this.userData);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onGoTo(sub: Subject): void {
    this.selectSubject = sub;
    // console.log(this.selectSubject);
    this.home.setSubject(this.selectSubject);
    if (this.home.getAction() === undefined) {
      this.home.toChaeckList(this.selectSubject);
      // console.log(this.selectSubject.status);
      if (this.selectSubject.status === "เจ้าของห้อง") {
        this.router.navigate(["/listCheck"]);
      } else {
        this.class.name = this.selectSubject.subject_name;
        this.class.group = this.selectSubject.group;
        this.opened = false;
        this.router.navigate(["/classMember"]);
      }
    }
    this.home.setAction(undefined);
  }
}
