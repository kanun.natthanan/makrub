import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ProfileService } from "../service/profile.service";
import { UserService } from "../service/user.service";
import { HomeServiceService } from "../home/home-service.service";

import { Subject } from "../card-detail";
export interface Room {
  name: string;
  group: string;
}

export interface UserData {
  username: string;
  nameEN: string;
  nameTH: string;
  university: string;
  faculty: string;
  img: string;
}

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"]
})
export class ProfileComponent implements OnInit {
  constructor(
    private router: Router,
    private profile: ProfileService,
    private user: UserService,
    private homeInService: HomeServiceService
  ) {}
  opened = false;
  imageUrl: string = "";
  USERDATA: UserData;
  selectSubject: Subject;
  subjects: Subject[];

  rooms: Room[] = [
    { name: "88625159 Mobile Programing", group: "2" },
    { name: "88625159 Web Programing", group: "2" }
  ];

  ngOnInit() {
    this.subjects = this.homeInService.getSubjects();
    // console.log(this.subjects);
    // console.log(this.user.getUser());
    // console.log(this.profile.getProfile(this.user.getUser()));
    this.USERDATA = this.profile.getProfile(this.user.getUser());
    this.profile.getImage(this.user.getUser()).subscribe(
      (res) => {
        if (res["img"].length == 0) {
          this.imageUrl = "../../assets/img/people.png";
        } else {
          this.imageUrl = "http://thebeach.buu.in.th:4011/images/" + res["img"];
        }
      },
      (err) => {
        console.log("IMG ERROR")
      }
    );
  }
  onLogout() {
    this.router.navigate(["/"]);
    localStorage.clear();
  }
  onProfile() {
    this.router.navigate(["/profile"]);
  }
  onHome() {
    this.router.navigate(["/home"]);
  }
  onSelect(sub: Subject): void {
    this.selectSubject = sub;
    this.homeInService.setSubject(this.selectSubject);
    if (this.homeInService.getAction() === undefined) {
      this.homeInService.toChaeckList(this.selectSubject);
      // console.log(this.selectSubject.status);
      if (this.selectSubject.status === "เจ้าของห้อง") {
        this.router.navigate(["/listCheck"]);
      } else {
        this.router.navigate(["/classMember"]);
      }
    }
    this.homeInService.setAction(undefined);
  }
}
