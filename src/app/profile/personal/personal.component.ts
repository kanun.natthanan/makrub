import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserService } from "src/app/service/user.service";
import { ProfileService } from "src/app/service/profile.service";
import { UpdateProfileDialog } from "./personal.dialog";
import { MatDialog } from "@angular/material";
import { ProfileComponent } from "../profile.component";

export interface UserData {
  username: string;
  nameEN: string;
  nameTH: string;
  university: string;
  faculty: string;
  img: string;
}

@Component({
  selector: "app-personal",
  templateUrl: "./personal.component.html",
  styleUrls: ["./personal.component.scss"],
})
export class PersonalComponent implements OnInit {
  //image
  imageUrl: string = "";
  fileToUpload: File = null;
  img = {
    images: "",
  };
  fd = new FormData();

  //profileUser
  USERDATA: UserData;

  private firstName = "";
  private lastName = "";

  constructor(
    private router: Router,
    private user: UserService,
    private profile: ProfileService,
    private profileCom: ProfileComponent,
    public dialog: MatDialog
  ) {}
  opened = false;

  ngOnInit() {
    // console.log(this.profile.getProfile(this.user.getUser()));
    this.USERDATA = this.profile.getProfile(this.user.getUser());
    this.profile.getImage(this.user.getUser()).subscribe(
      (res) => {
        if (res["img"].length == 0) {
          this.imageUrl = "../../assets/img/people.png";
        } else {
          this.imageUrl = "http://thebeach.buu.in.th:4011/images/" + res["img"];
        }
      },
      (err) => {
        console.log("IMG ERROR");
      }
    );
  }

  handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);
    this.fd.append(
      "file",
      this.fileToUpload,
      this.USERDATA.username + "." + this.fileToUpload.name
    );
    // console.log(file.item(0).name);

    //show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    };
    reader.readAsDataURL(this.fileToUpload);
    reader.EMPTY;
  }

  updateProfile() {
    try {
      if (this.USERDATA.nameTH.substring(0, 3) === "นาย") {
        this.USERDATA.nameTH = this.USERDATA.nameTH.substr(3);
        this.firstName = this.USERDATA.nameTH.slice(
          0,
          this.USERDATA.nameTH.indexOf(" ")
        );
        this.lastName = this.USERDATA.nameTH.slice(
          this.USERDATA.nameTH.indexOf(" ") + 1,
          this.USERDATA.nameTH.length
        );
      } else if (this.USERDATA.nameTH.substring(0, 6) === "นางสาว") {
        this.USERDATA.nameTH = this.USERDATA.nameTH.substr(6);
        this.firstName = this.USERDATA.nameTH.slice(
          0,
          this.USERDATA.nameTH.indexOf(" ")
        );
        this.lastName = this.USERDATA.nameTH.slice(
          this.USERDATA.nameTH.indexOf(" ") + 1,
          this.USERDATA.nameTH.length
        );
      } else if (this.USERDATA.nameTH.substring(0, 3) === "นาง") {
        this.USERDATA.nameTH = this.USERDATA.nameTH.substr(3);
        this.firstName = this.USERDATA.nameTH.slice(
          0,
          this.USERDATA.nameTH.indexOf(" ")
        );
        this.lastName = this.USERDATA.nameTH.slice(
          this.USERDATA.nameTH.indexOf(" ") + 1,
          this.USERDATA.nameTH.length
        );
      } else {
        this.firstName = this.USERDATA.nameTH.slice(
          0,
          this.USERDATA.nameTH.indexOf(" ")
        );
        this.lastName = this.USERDATA.nameTH.slice(
          this.USERDATA.nameTH.indexOf(" ") + 1,
          this.USERDATA.nameTH.length
        );
      }
      // console.log(`${firstName} // ${lastName}`)

      this.updateProfileDialog();
    } catch (error) {
      console.log("ERROR NOT TEMPLETE");
    }
  }
  updateProfileDialog() {
    // console.log(`${this.firstName} // ${this.lastName}`);
    const dialogRef = this.dialog.open(UpdateProfileDialog, {
      panelClass: "custom-dialog-container",
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result == "confirm") {
        this.profile.updateNameTH(
          this.USERDATA.username,
          this.firstName,
          this.lastName
        );
        if (this.fileToUpload != null) {
          this.profile.uploadImg(this.fd);
        }
        this.USERDATA = this.profile.getProfile(this.user.getUser());
        // console.log(this.USERDATA);
        this.profileCom.imageUrl = this.imageUrl;
      } else {
        this.ngOnInit();
      }
    });
  }
}
