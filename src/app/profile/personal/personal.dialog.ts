import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
@Component({
    templateUrl: "updateProfileDialog.html",
    styleUrls: ["./personal.component.scss"]
  })
  export class UpdateProfileDialog {
    constructor(
      public dialogRef: MatDialogRef<UpdateProfileDialog>,
    ) {}
  
    onConfirm(): void {
      this.dialogRef.close("confirm");
    }
    onClose(): void {
      this.dialogRef.close();
    }
  }