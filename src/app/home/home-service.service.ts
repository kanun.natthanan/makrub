import { Injectable } from "@angular/core";
import { Subject } from "../card-detail";

@Injectable({
  providedIn: "root"
})
export class HomeServiceService {
  private subject: Subject;
  private subjects: Subject[];
  private action: String;
  private data = {
    name: "",
    id: "",
    group: "",
    detail: "",
    code: ""
  };

  constructor() {}
  setSubject(value) {
    this.subject = value;
  }
  setAction(value) {
    this.action = value;
  }
  setSubjects(values) {
    this.subjects = values;
    localStorage.setItem(".subjects", JSON.stringify(this.subjects));
  }
  getSubjects() {
    let data = localStorage.getItem(".subjects");
    return JSON.parse(data);
  }
  getSubject() {
    return this.subject;
  }
  getAction() {
    return this.action;
  }
  toChaeckList(sub) {
    // console.log(sub);
    this.data.name = sub["subject_name"];
    this.data.group = sub["group"];
    this.data.id = sub["idpk"];
    this.data.detail = sub["detail"];
    this.data.code = sub["code"];
    localStorage.setItem(".data", JSON.stringify(this.data));
  }
  getData() {
    let data = localStorage.getItem(".data");
    return JSON.parse(data);
  }
}
