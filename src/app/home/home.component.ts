import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Subject } from "../card-detail";
import { HomeServiceService } from "./home-service.service";
import { MatDialog } from "@angular/material/dialog";
import {
  DeleteDialog,
  JoinClassDialog,
  EditDialog,
  AddDialog,
} from "./home.dialog";
import { UserService } from "../service/user.service";
import { HomeService } from "../service/home.service";
import { ProfileService } from "../service/profile.service";
// import { SUBJECTS } from "../mockup";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {
  subjects: Subject[];
  subjects_new: Subject[];
  selectSubject: Subject;
  imageUrl = "";
  private selected;
  private subjects_for_search;
  status: string;

  constructor(
    private router: Router,
    private homeInService: HomeServiceService,
    public dialog: MatDialog,
    private userService: UserService,
    private homeService: HomeService,
    private profile: ProfileService
  ) {}
  opened = false;

  ngOnInit() {
    this.subjects = [];
    this.reload("start", null);
    this.profile.getImage(this.userService.getUser()).subscribe(
      (res) => {
        if (res["img"].length == 0) {
          this.imageUrl = "../../assets/img/people.png";
        } else {
          this.imageUrl = "http://thebeach.buu.in.th:4011/images/" + res["img"];
        }
      },
      (err) => {
        console.log("IMG ERROR");
      }
    );
  }
  onLogout() {
    this.router.navigate(["/"]);
  }
  onProfile() {
    this.router.navigate(["/profile"]);
  }
  onHome() {
    this.router.navigate(["/home"]);
  }
  onSelect(sub: Subject): void {
    this.selectSubject = sub;
    // console.log(sub);
    this.homeInService.setSubject(this.selectSubject);
    if (
      this.homeInService.getAction() === undefined ||
      this.homeInService.getAction() === "chart"
    ) {
      this.homeInService.toChaeckList(this.selectSubject);
      // console.log(this.selectSubject.status);
      if (this.homeInService.getAction() === "chart") {
        if (this.selectSubject.status === "เจ้าของห้อง") {
          this.router.navigate(["/reportResult"]);
        } else {
          this.router.navigate(["/classMember"]);
        }
      } else {
        if (this.selectSubject.status === "เจ้าของห้อง") {
          this.router.navigate(["/listCheck"]);
        } else {
          this.router.navigate(["/classMember"]);
        }
      }
    } else if (this.homeInService.getAction() === "delete") {
      //DELETE
      const dialogRef = this.dialog.open(DeleteDialog, {
        panelClass: "custom-dialog-container",
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result == "delete") {
          // console.log(sub["store"]);
          // console.log(sub["idpk"]);
          this.subjects_for_search = [];
        }
        this.reload("delete", result);
      });
    } else {
      //EDIT
      const dialogRef = this.dialog.open(EditDialog, {
        panelClass: "custom-dialog-long",
      });
      dialogRef.afterClosed().subscribe((result) => {
        // console.log(result)
        this.reload("edit", result);
      });
    }
    this.homeInService.setAction(undefined);
  }
  addClass() {
    const dialogRef = this.dialog.open(AddDialog, {
      panelClass: "custom-dialog-long",
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.reload("add", null);
    });
  }

  reload(action, value) {
    this.subjects_new = [];
    this.homeService.getHome(this.userService.getUser()).subscribe((res) => {
      for (var x in res) {
        for (var y in res[x].member) {
          if (this.userService.getUser() === res[x].member[y].username) {
            status = res[x].member[y].status;
            // console.log(status);
          }
        }
        if (action === "start") {
          this.subjects.push({
            group: res[x].group,
            status: status,
            pic_id: res[x].pic_id,
            subject_name: res[x].name,
            idpk: res[x]._id,
            detail: res[x].detail,
            code: res[x].code,
            sort: res[x].numberClass,
            store: res[x].status,
          });
        } else if (action === "delete" || action === "add") {
          // console.log(res);
          this.subjects_new.push({
            group: res[x].group,
            status: status,
            pic_id: res[x].pic_id,
            subject_name: res[x].name,
            idpk: res[x]._id,
            detail: res[x].detail,
            code: res[x].code,
            sort: res[x].numberClass,
            store: res[x].status,
          });
        } else if (action === "edit" && value != null) {
          if (res[x]._id === value) {
            this.subjects.splice(parseInt(x), 1);
            this.subjects.push({
              group: res[x].group,
              status: status,
              pic_id: res[x].pic_id,
              subject_name: res[x].name,
              idpk: res[x]._id,
              detail: res[x].detail,
              code: res[x].code,
              sort: res[x].numberClass,
              store: res[x].status,
            });
            this.subjects.sort(function sortt(a, b) {
              return a.sort - b.sort;
            });
            break;
          }
        } else {
          for (var z in res) {
            if (res[z].code === action) {
              status = "สมาชิก";
              this.subjects.push({
                group: res[z].group,
                status: status,
                pic_id: res[z].pic_id,
                subject_name: res[z].name,
                idpk: res[z]._id,
                detail: res[z].detail,
                code: res[z].code,
                sort: res[z].numberClass,
                store: res[z].status,
              });
            }
            this.subjects.sort(function sortt(a, b) {
              return a.sort - b.sort;
            });
          }
          // console.log(action);
          break;
        }
      }

      if (
        this.subjects.length > this.subjects_new.length &&
        action === "delete"
      ) {
        var deleted: String;
        // console.log("DELETED");
        try {
          for (x in this.subjects) {
            if (this.subjects[x].idpk === this.subjects_new[x].idpk) {
              // console.log(
              //   `${this.subjects[x].idpk} === ${this.subjects_new[x].idpk}`
              // );
            } else {
              // console.log(`${this.subjects[x].idpk} Deleted `);
              deleted = this.subjects[x].idpk;
              break;
            }
          }
        } catch (error) {
          // console.log(
          //   `${this.subjects[this.subjects.length - 1].idpk} Deleted `
          // );
          deleted = this.subjects[x].idpk;
        }
        // console.log(this.subjects);
        for (x in this.subjects) {
          if (this.subjects[x].idpk === deleted) {
            this.subjects.splice(parseInt(x), 1);
            break;
          }
        }
      } else if (
        this.subjects.length < this.subjects_new.length &&
        action === "add"
      ) {
        // console.log(this.subjects);
        // console.log(this.subjects_new);
        this.subjects.push(this.subjects_new.pop());
      } else if (action === "delete" && value != "cancel") {
        this.subjects.forEach((element, index) => {
          if (element["idpk"] === this.selectSubject["idpk"]) {
            this.subjects.splice(index, 1);
          }
        });
      }
      this.homeInService.setSubjects(this.subjects);
      this.subjects_for_search = this.subjects;
    });
  }
  joinClass() {
    const dialogRef = this.dialog.open(JoinClassDialog, {
      panelClass: "custom-dialog-container",
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.reload(result, null);
    });
  }
  selectStatus() {
    this.subjects = this.subjects_for_search;
    if (this.selected === "1") {
      this.subjects = [];
      // console.log(this.subjects_for_search);
      for (let i in this.subjects_for_search) {
        this.subjects.push(this.subjects_for_search[i]);
      }
    } else if (this.selected === "2") {
      this.subjects_new = this.subjects.filter(function member(status) {
        return status.status === "เจ้าของห้อง";
      });
      this.subjects = [];
      for (let i in this.subjects_new) {
        this.subjects.push(this.subjects_new[i]);
      }
    } else {
      this.subjects_new = this.subjects.filter(function member(status) {
        return status.status === "สมาชิก";
      });
      this.subjects = [];
      for (let i in this.subjects_new) {
        this.subjects.push(this.subjects_new[i]);
      }
    }
  }
}
