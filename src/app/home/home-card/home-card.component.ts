import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { Subject } from "../../card-detail";
import { HomeServiceService } from "../home-service.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-home-card",
  templateUrl: "./home-card.component.html",
  styleUrls: ["./home-card.component.scss"]
})
export class HomeCardComponent implements OnInit {
  @Input() detailData: Subject;
  netImage: any = `../../../assets/img/card-header/card-head-2.png`;
  private color: String;
  private isDisabled: boolean = true;
  private isStore: boolean = false;
  private gray : String = "0"
  constructor(
    private homeService: HomeServiceService,
    private router: Router
  ) {}

  ngOnInit() {
    this.isDisabled = false;
    // console.log(this.detailData.status);
    if (this.detailData.pic_id.length > 1) {
      this.netImage = `../../../assets/img/card-header/card-head-9.png`;
      this.color = `hsl(${this.detailData.pic_id}, 70%, 36%)`;
    } else {
      this.netImage = `../../../assets/img/card-header/card-head-${this.detailData.pic_id}.png`;
    }
    if (this.detailData.store === "true") {
      // console.log("NOT STORE");
      if (this.detailData.status === "สมาชิก") {
        this.isDisabled = true;
      }
    } else {
      // console.log("STORE");
      this.isDisabled = true;
      this.isStore = true;
      this.gray = "100"
    }
  }
  delete() {
    this.homeService.setAction("delete");
  }
  edit() {
    this.homeService.setAction("edit");
  }
  chart() {
    this.homeService.setAction("chart")
  }
  getBackgroundColor() {
    return this.color;
  }
  restore() {
    this.homeService.setAction("restore");
  }
  delete100per() {
    this.homeService.setAction("delete100");
  }
}
