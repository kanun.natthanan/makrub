import { Component, OnInit, Inject } from "@angular/core";
import { Subject } from "../card-detail";
import { HomeServiceService } from "./home-service.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { HomeService } from "../service/home.service";
import { UserService } from "../service/user.service";
import { SUBJECTS } from "../mockup";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators
} from "@angular/forms";
import { ErrorStateMatcher } from "@angular/material/core";

// ///////////////////// DELETE.CM
@Component({
  templateUrl: "deleteDialog.html",
  styleUrls: ["./home.component.scss"]
})
export class DeleteDialog {
  subjects: Subject[];
  constructor(
    public dialogRef: MatDialogRef<DeleteDialog>,
    private homeInService: HomeServiceService,
    private homeService: HomeService,
    private userService: UserService
  ) {}

  onNoClick(): void {
    this.dialogRef.close("cancel");
  }
  onDelete() {
    // console.log(this.homeInService.getSubject().idpk);
    this.homeService.deleteClass(this.homeInService.getSubject().idpk);
    this.dialogRef.close("delete");
  }
}
// /////////////////////////////// /EDIT /////
@Component({
  templateUrl: "editDialog.html",
  styleUrls: ["./home.component.scss"]
})
export class EditDialog {
  private subjects: Subject[];
  private nameRoom: String;
  private detailRoom: String;
  private group: String;
  private color: String = "hsl(182, 70%, 36%)";
  private c: String = "182";
  private isDisabled: boolean = true;
  private picker: String = "1";
  private subject: number = 0;
  private subject_new: Subject;
  nameList: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<EditDialog>,
    private homeInService: HomeServiceService,
    private homeService: HomeService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}
  ngOnInit() {
    this.nameList = this.formBuilder.group({
      name: ["", [Validators.required]]
    });
    this.subjects = this.homeInService.getSubjects();
    for (var read in this.subjects) {
      if (
        this.subjects[this.subject].idpk ===
        this.homeInService.getSubject().idpk
      ) {
        break;
      }
      this.subject++;
    }
    this.nameRoom = this.subjects[this.subject].subject_name;
    this.group = this.subjects[this.subject].group;
    this.detailRoom = this.subjects[this.subject].detail;
    this.picker = this.subjects[this.subject].pic_id;
    if (this.subjects[this.subject].pic_id.length > 1) {
      this.picker = "9";
      this.c = this.subjects[this.subject].pic_id;
      this.setBackgroundColor(this.c);
      this.isDisabled = false;
    } else {
      this.picker = this.subjects[this.subject].pic_id;
    }
  }
  getBackgroundColor() {
    return this.color;
  }
  setBackgroundColor(c) {
    this.color = `hsl(${c}, 70%, 36%)`;
  }
  onItemChange() {
    // console.log(this.picker);
    if (this.picker === "9") {
      this.isDisabled = false;
    } else {
      this.isDisabled = true;
    }
  }
  onItemClick() {
    if (this.picker === "9") {
      this.picker = "0";
    } else {
      this.isDisabled = true;
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  btnConfirm() {
    if (this.nameList.status != "VALID") {
    } else {
      if (this.picker === "9") {
        this.subjects[this.subject].pic_id = this.c.toString();
      } else {
        this.subjects[this.subject].pic_id = this.picker;
      }
      this.subjects[this.subject].subject_name = this.nameRoom;
      this.subjects[this.subject].detail = this.detailRoom;
      this.subjects[this.subject].group = this.group;
      this.subject_new = this.subjects[this.subject];
      this.subjects.push({
        group: this.subject_new.group,
        status: this.subject_new.status,
        pic_id: this.subject_new.pic_id,
        subject_name: this.subject_new.subject_name,
        idpk: this.subject_new.idpk,
        detail: this.subject_new.detail,
        code: this.subject_new.code,
        sort: this.subject_new.sort,
        store: this.subject_new.store
      });
      this.homeService.editClass(this.subject_new);
      this.dialogRef.close(this.subject_new.idpk);
    }
  }
}
//////////////////////////////////////////////////////////////////////ADD.CM//
@Component({
  templateUrl: "addClassDialog.html",
  styleUrls: ["./home.component.scss"]
})
export class AddDialog {
  private nameRoom: String;
  private detailRoom: String;
  private group: String;
  private color: String = "hsl(182, 70%, 36%)";
  private c: String;
  private isDisabled: boolean = true;
  private picker: String = "1";
  private subject_add: Subject;
  nameList: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AddDialog>,
    private homeService: HomeService,
    private userService: UserService,
    private formBuilder: FormBuilder,

    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}
  ngOnInit() {
    this.nameList = this.formBuilder.group({
      name: ["", [Validators.required]]
    });
  }
  getBackgroundColor() {
    return this.color;
  }
  setBackgroundColor() {
    this.color = `hsl(${this.c}, 70%, 36%)`;
  }
  onItemChange() {
    // console.log(this.picker);
    if (this.picker === "9") {
      this.isDisabled = false;
    } else {
      this.isDisabled = true;
    }
  }
  onItemClick() {
    // console.log(this.picker);
    if (this.picker === "9") {
      this.picker = "0";
    } else {
      this.isDisabled = true;
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  btnConfirm() {
    if (this.nameList.status != "VALID") {
    } else {
      this.subject_add = {
        group: "",
        status: "",
        pic_id: "",
        subject_name: "",
        idpk: "",
        detail: "",
        code: "",
        sort: 0,
        store: ""
      };
      if (this.picker === "9") {
        this.subject_add.pic_id = this.c.toString();
        // console.log(`You Select background color ${this.color}`);
      } else {
        this.subject_add.pic_id = this.picker;
        // console.log(`You Select background no.${this.picker}`);
      }
      this.subject_add.group = this.group;
      this.subject_add.detail = this.detailRoom;
      this.subject_add.subject_name = this.nameRoom;
      this.homeService.addClass(this.subject_add);
      this.dialogRef.close();
    }
  }
}

@Component({
  templateUrl: "joinClassDialog.html",
  styleUrls: ["./home.component.scss"]
})
export class JoinClassDialog {
  subjects: Subject[];
  private code: String;
  constructor(
    public dialogRef: MatDialogRef<JoinClassDialog>,
    private homeInService: HomeServiceService,
    private homeService: HomeService,
    private userService: UserService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  onSubmit() {
    // console.log(this.code);
    var model = {
      code: this.code,
      username: this.userService.getUser()
    };
    this.homeService.joinClass(model);
    this.dialogRef.close(this.code);
  }
}
