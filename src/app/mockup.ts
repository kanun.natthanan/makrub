import { Subject } from "./card-detail";

export const SUBJECTS: Subject[] = [
  {
    group: "0001",
    status: "owner",
    pic_id: "1",
    subject_name: "Logical Thinking",
    idpk: "111",
    detail: "ALALAL",
    code :"dd",
    sort : 1,
    store : "true"
  },
  {
    group: "0002",
    status: "owner",
    pic_id: "2",
    subject_name: "Logical Thinking",
    idpk: "112",
    detail: "ALALAL",
    code :"dd",
    sort : 2,
    store : "true"
  },
  {
    group: "0003",
    status: "owner",
    pic_id: "3",
    subject_name: "Logical Thinking",
    idpk: "113",
    detail: "ALALAL",
    code :"dd",
    sort : 3,
    store : "true"
  },
  {
    group: "0004",
    status: "owner",
    pic_id: "4",
    subject_name: "Logical Thinking",
    idpk: "114",
    detail: "ALALAL",
    code :"dd",
    sort : 4,
    store : "true"
  },
  {
    group: "0005",
    status: "DELETE",
    pic_id: "4",
    subject_name: "Logical Thinking",
    idpk: "115",
    detail: "ALALAL",
    code :"dd",
    sort : 5,
    store : "true"
  },
  {
    group: "0006",
    status: "owner",
    pic_id: "5",
    subject_name: "Logical Thinking",
    idpk: "116",
    detail: "ALALAL",
    code :"dd",
    sort : 6,
    store : "true"
  },
  {
    group: "0007",
    status: "owner",
    pic_id: "6",
    subject_name: "Logical Thinking",
    idpk: "117",
    detail: "ALALAL",
    code :"dd",
    sort : 7,
    store : "true"
  },
  {
    group: "0008",
    status: "user",
    pic_id: "66",
    subject_name: "Logical Thinking",
    idpk: "118",
    detail: "ALALAL",
    code :"dd",
    sort : 8,
    store : "true"
  },
  {
    group: "0009",
    status: "user",
    pic_id: "8",
    subject_name: "Logical Thinking",
    idpk: "119",
    detail: "ALALAL",
    code :"dd",
    sort : 9,
    store : "true"
  }
];
