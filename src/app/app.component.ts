import { Component } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Makrub';
  routerUrl : string

  constructor(private router: Router) { 
    this.router.events.subscribe((res) => { 
      this.routerUrl = this.router.url;
  })
  }
  ngOnInit() {
    
  }
  

}

