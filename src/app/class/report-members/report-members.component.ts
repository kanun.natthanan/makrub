import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material'
import { ClassService } from 'src/app/service/class.service';
import { ReportService } from 'src/app/service/report.service';
import { HomeServiceService } from 'src/app/home/home-service.service';

export interface UserData {
  id: number;
  list: string;
  type: string;
  hours: number;
  date: string;
  statusCheck: string;
}
export interface SumReport{
  onTime: number;
  late: number;
  absent: number;
}



@Component({
  selector: 'app-report-members',
  templateUrl: './report-members.component.html',
  styleUrls: ['../class.component.scss']
})


export class ReportMembersComponent implements OnInit {

  opened = false;
  userData: UserData[];
  reportMember: string;
  pathBefore: string;
  sumReport:SumReport = {onTime:0,late:0,absent:0};


  displayedColumns: string[] = ['id', 'list', 'type', 'hours', 'date', 'statusCheck'];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;


  constructor(private router: Router,
    public member: ClassService,
    private report: ReportService,
    private home: HomeServiceService) {

  }
  ngOnInit() {
    this.reportMember = this.member.getReportMember().name
    this.pathBefore =  this.member.getPathBefore().name
    this.createTable()
  }
  createTable() {
    this.report.getReportMember(this.home.getData().id, this.member.getReportMember().username).subscribe(
      res => {
        var round = 0;
        this.userData = [];
        var status = ""
        // console.log(res)
        for (var x in res) {
          round++;
          if (res[x].status == "onTime") {
            status = "ทันเวลา"
            this.sumReport.onTime = this.sumReport.onTime+1;
          } else if (res[x].status == "late") {
            status = "สาย"
            this.sumReport.late = this.sumReport.late+1;
          } else if (res[x].status == "leave") {
            status = "ลากิจ"
            this.sumReport.absent = this.sumReport.absent+1
          } else if (res[x].status == "sick") {
            status = "ลาป่วย"
            this.sumReport.absent = this.sumReport.absent+1
          } else if (res[x].status == "notSendLeave") {
            status = "ไม่ส่งใบลา"
            this.sumReport.absent = this.sumReport.absent+1
          } else if (res[x].status == "empty") {
            status = "-"
            this.sumReport.absent = this.sumReport.absent+1
          }
          this.userData.push({
            id: round,
            list: res[x].name,
            type: res[x].type,
            hours: res[x].hour,
            date: res[x].date,
            statusCheck: status,
          });

        }


        this.getTable()
      },
      err => {
        console.log("CREATE TABLE ERROR");

      }
    );
  }
  getTable() {
    this.dataSource = new MatTableDataSource(this.userData);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }

  onLogout() {
    localStorage.clear();
    this.router.navigate(["/"]);
  }
  onProfile() {
    this.router.navigate(["/profile"]);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}

