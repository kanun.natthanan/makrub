import { Component, OnInit,ViewChild } from '@angular/core';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort} from '@angular/material/sort';
import { MatTableDataSource} from '@angular/material/table';
import { MatDialog } from '@angular/material'
import { ClassService } from 'src/app/service/class.service';
import { ReportService } from 'src/app/service/report.service';
import { ListcheckServiceService } from '../../listcheck/listcheck-service.service';



export interface UserData {
  id: number;
  username: string;
  name: string;
  statusCheck: string;
}
export interface  member{
  username: string;
  name: string;
}

export interface SumReport{
  onTime: number;
  late: number;
  absent: number;
}


@Component({
  selector: 'app-report-list',
  templateUrl: './report-list.component.html',
  styleUrls: ['./report-list.component.scss']
})
export class ReportListComponent implements OnInit {

  userData: UserData[];
  reportMember: member;
  sumReport:SumReport = {onTime:0,late:0,absent:0};

  displayedColumns: string[] = ['id', 'username', 'name', 'statusCheck'];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(public list:ClassService,
    private report: ReportService,
    private listCheckSer: ListcheckServiceService,) { 
   
  }
  ngOnInit() {
    this.createTable()
  }
  createTable() {
    this.report.getReportList(this.list.getTitleList().id).subscribe(
      res => {
        var round = 0;
        var status= "";
        this.userData = [];
        // console.log(res)
        for (var x in res) {
          round++;
          if (res[x].status == "onTime") {
            status = "ทันเวลา"
            this.sumReport.onTime = this.sumReport.onTime+1;
          } else if (res[x].status == "late") {
            status = "สาย"
            this.sumReport.late = this.sumReport.late+1;
          } else if (res[x].status == "leave") {
            status = "ลากิจ"
            this.sumReport.absent = this.sumReport.absent+1
          } else if (res[x].status == "sick") {
            status = "ลาป่วย"
            this.sumReport.absent = this.sumReport.absent+1
          } else if (res[x].status == "notSendLeave") {
            status = "ไม่ส่งใบลา"
            this.sumReport.absent = this.sumReport.absent+1
          } else if (res[x].status == "empty") {
            status = "-"
            this.sumReport.absent = this.sumReport.absent+1
          }
          this.userData.push({
            id: round,
            username: res[x].username,
            name: res[x].firstnameTh+" "+res[x].lastnameTh,
            statusCheck: status

          });

        }
        this.getTable()
      },
      err => {
        console.log("CREATE TABLE ERROR");

      }
    );
  }
  getTable() {
    this.dataSource = new MatTableDataSource(this.userData);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }
 
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onClickUpdateReportMember(member){
    this.reportMember = {username:member.username,name:member.name}
    localStorage.setItem(".reportMember",JSON.stringify(this.reportMember))

    localStorage.setItem(".pathBefore",JSON.stringify({name:this.list.getTitleList().name}))

  }

}

