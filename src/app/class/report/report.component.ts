import { Component, OnInit,ViewChild } from '@angular/core';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort} from '@angular/material/sort';
import { MatTableDataSource} from '@angular/material/table';
import { MatDialog } from '@angular/material'
import { ClassService } from 'src/app/service/class.service';
import { ReportService } from 'src/app/service/report.service';
import { HomeServiceService } from 'src/app/home/home-service.service';
import { ListcheckService } from 'src/app/service/listcheck.service';
import { ListcheckServiceService } from '../listcheck/listcheck-service.service';

export interface ListData {
  id: number;
  idpk: string;
  list: string;
  type: string;
  hours: number;
  date: string;
  countMember: number;
  onTime: number;
  late: number;
  leave: number;
  sick: number;
  notSendLeave: number;

 
}
export interface list{
  id: string;
  name: string;
}

export interface States {
  value: string;
  viewValue: string;
}




@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  listData:ListData[] ;
  titleList: list;

  states: States[] = [{value: 'ทั้งหมด',viewValue: ''},
  {value: 'บรรยาย',viewValue: 'บรรยาย'},{value: 'ปฏิบัติการ',viewValue: 'ปฏิบัติการ'},{value: 'กิจกรรมอื่น',viewValue: 'กิจกรรมอื่น'}];
  

  displayedColumns: string[] = ['id', 'list', 'type', 'hours','date','countMember','onTime','late','leave','sick','notSendLeave'];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  dataSource: MatTableDataSource<ListData>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(public list:ClassService,
    private report: ReportService,
    private home: HomeServiceService,
    private listcheck: ListcheckService,
    private listcheckSer:ListcheckServiceService) { 

  }

  ngOnInit() {
    this.createTable()
  }
  createTable() {
    this.report.getReportAll(this.home.getData().id).subscribe(
      res => {
        var round = 0;
        this.listData = [];
        // console.log(res)
        for (var x in res) {
          round++;
          this.listData.push({
            id: round,
            idpk: res[x]._id,
            list: res[x].name,
            type: res[x].type,
            hours: Number(res[x].hour),
            date: res[x].date,
            countMember: Number(res[x].countMember),
            onTime: Number(res[x].onTime),
            late: Number(res[x].late),
            leave: Number(res[x].leave),
            sick: Number(res[x].sick),
            notSendLeave: Number(res[x].notSendLeave)
          });
          
        }
        this.getTable()
        
      },
      err => {
        console.log("CREATE TABLE ERROR");

      }
    );
   

  }
  getTable() {
    this.dataSource = new MatTableDataSource(this.listData);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }
 
  applyFilter(filterValue: string) {
    

    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onClickupdateTitleList(title,idpk){
    this.titleList = {id:idpk,name:title}
    localStorage.setItem(".titleList",JSON.stringify(this.titleList))
    this.listcheckSer.setIdList(idpk);
  }
 

}
