import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { HomeServiceService } from "../home/home-service.service";
import { ProfileService } from "../service/profile.service";
import { UserService } from "../service/user.service";
import { Subject } from "../card-detail";

export interface Room {
  name: string;
  group: string;
}

export interface ClassRoom {
  name: string;
  group: string;
  detail: string;
}

@Component({
  selector: "app-class",
  templateUrl: "./class.component.html",
  styleUrls: ["./class.component.scss"]
})
export class ClassComponent implements OnInit {
  opened = false;
  links = ["listCheck", "members", "report"];
  menus = ["รายการเช็คชื่อ", "จัดการรายชื่อ", "รายงานการเช็คชื่อ"];
  rooms: Room[] = [
    { name: "88625159 Mobile Programing", group: "2" },
    { name: "88625159 Web Programing", group: "2" }
  ];
  classRoom: ClassRoom = { name: "", detail: "", group: "" };

  imageUrl : String = "";
  private subjects_new;
  private selectSubject;

  constructor(
    private router: Router,
    private home: HomeServiceService,
    private profile: ProfileService,
    private user: UserService
  ) {}

  ngOnInit() {
    this.subjects_new = this.home.getSubjects();
    this.classRoom.name = this.home.getData().name;
    this.classRoom.detail = this.home.getData().detail;
    this.classRoom.group = this.home.getData().group;
    this.profile.getImage(this.user.getUser()).subscribe(
      (res) => {
        if (res["img"].length == 0) {
          this.imageUrl = "../../assets/img/people.png";
        } else {
          this.imageUrl = "http://thebeach.buu.in.th:4011/images/" + res["img"];
        }
      },
      (err) => {
        console.log("IMG ERROR")
      }
    );
  }

  onLogout() {
    localStorage.clear();
    this.router.navigate(["/"]);
  }
  onProfile() {
    this.router.navigate(["/profile"]);
  }
  onGoTo(sub: Subject): void {
    this.selectSubject = sub;
    this.home.setSubject(this.selectSubject);
    if (this.home.getAction() === undefined) {
      this.home.toChaeckList(this.selectSubject);
      if (this.selectSubject.status === "เจ้าของห้อง") {
        this.opened = false;
        this.subjects_new = this.home.getSubjects();
        this.classRoom.name = this.home.getData().name;
        this.classRoom.detail = this.home.getData().detail;
        this.classRoom.group = this.home.getData().group;
        this.router.navigate(["/listCheck"]);
      } else {
        this.router.navigate(["/classMember"]);
      }
    }
    this.home.setAction(undefined);
  }
 
}

