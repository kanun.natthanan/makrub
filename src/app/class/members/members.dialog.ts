import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { HomeServiceService } from "src/app/home/home-service.service";
import { Component, OnInit } from "@angular/core";
import { ListcheckServiceService } from "../listcheck/listcheck-service.service";
import { MembersService } from "src/app/service/members.service";
import * as XLSX from "xlsx";
import { UserService } from "src/app/service/user.service";
import { ProfileService } from "src/app/service/profile.service";
import { saveAs } from "file-saver";

export interface StateGroup {
  letter: string;
  names: string[];
}

export const _filter = (opt: string[], value: string): string[] => {
  const filterValue = value.toLowerCase();

  return opt.filter((item) => item.toLowerCase().indexOf(filterValue) === 0);
};

@Component({
  templateUrl: "codeDialog.html",
  styleUrls: ["members.component.scss"],
})
export class CodeDialog {
  code: string = "";
  room: string = "88636459  Mobile Developer กลุ่ม 1";
  constructor(
    public dialogRef: MatDialogRef<CodeDialog>,
    private home: HomeServiceService
  ) {
    this.code = this.home.getData().code;
    this.room =
      this.home.getData().name +
      " " +
      this.home.getData().detail +
      " กลุ่ม " +
      this.home.getData().group;
  }

  onClose(): void {
    this.dialogRef.close();
  }
}
type AOA = any[][];
@Component({
  templateUrl: "importMembersDialog.html",
  styleUrls: ["members.component.scss"],
})
export class ImportMembersDialog {
  fileToUpload: File = null;
  filename: string = "นำเข้ารายชื่อ";
  private name = "";
  private username = "";

  constructor(
    private home: HomeServiceService,
    public dialogRef: MatDialogRef<ImportMembersDialog>,
    private memberService: MembersService,
    private user: UserService,
    private profile: ProfileService
  ) {}

  onClose(): void {
    this.dialogRef.close();
  }
  handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);
    this.filename = file.item(0).name;
  }
  data: AOA = [];
  wopts: XLSX.WritingOptions = { bookType: "xlsx", type: "array" };

  onFileChange(evt: any) {
    /* wire up file reader */
    // console.log(evt);
    const target: DataTransfer = <DataTransfer>evt.target;
    if (target.files.length !== 1) throw new Error("Cannot use multiple files");
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: "binary" });

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = <AOA>XLSX.utils.sheet_to_json(ws, { header: 1 });
      // console.log(this.data);
    };
    reader.readAsBinaryString(target.files[0]);
  }
  is_numeric(str) {
    return /^\d+$/.test(str);
  }
  onConfirm() {
    let members = [];
    for (let x in this.data) {
      if (this.data[x][0] == this.user.getUser() || x === "0") {
        continue;
      } else if (this.data[x][0] == undefined || this.data[x][0] == 'หมายเหตุ') {
        break;
      }
      let arr = [];
      let arr2 = [];
      let name = this.data[x][1].trim();
      arr2 = name.split(" ");
      // console.log(arr2);
      if (arr2.length < 2) {
        name = `${arr2[0]}  `;
      }
      // console.log(name)

      try {
        if (name.substring(0, 3) === "นาย") {
          name = name.substr(3);
          var f = name.slice(0, name.indexOf(" "));
          var l = name.slice(name.indexOf(" ") + 1, name.length);
          arr.push(f, l);
        } else if (name.substring(0, 6) === "นางสาว") {
          name = name.substr(6);
          var f = name.slice(0, name.indexOf(" "));
          var l = name.slice(name.indexOf(" ") + 1, name.length);
          arr.push(f, l);
        } else if (name.substring(0, 3) === "นาง") {
          name = name.substr(3);
          var f = name.slice(0, name.indexOf(" "));
          var l = name.slice(name.indexOf(" ") + 1, name.length);
          arr.push(f, l);
        } else{
          var f = name.slice(0, name.indexOf(" "));
          var l = name.slice(name.indexOf(" ") + 1, name.length);
          arr.push(f, l);
        }
        if (arr.length == 2) {
          members.push({
            username: this.data[x][0] + "",
            firstnameTh: arr[0],
            lastnameTh: arr[1].trim(),
          });
        }
      } catch (error) {
        console.log("ERROR NOT TEMPLETE");
      }
    }
    // console.log(members);
    this.insertOne(members);

    this.dialogRef.close();
  }
  export() {
    var FileSaver = saveAs;
    FileSaver.saveAs(
      "http://thebeach.buu.in.th:4011/excel/example_excel_insert.xlsx",
      "example_excel_insert.xlsx"
    );
  }

  insertOne(list) {
    // console.log(this.memberService.contactsList);
    if (list.length != 0) {
      var contact = [];
      for (var i = 0; i < list.length; i++) {
        // console.log(list[i].username);
        contact.push(list[i].username);
      }
      this.memberService.setContactsList(contact);
      // console.log(this.memberService.contactsList);
      this.memberService.addMemberExcel(list, this.home.getData().id).subscribe(
        (res) => {
          // console.log(res);
        },
        (err) => {
          // console.log(err);
        }
      );
    }
  }
}

@Component({
  templateUrl: "deleteMemberDialog.html",
  styleUrls: ["members.component.scss"],
})
export class DeleteMemberDialog {
  constructor(
    public dialogRef: MatDialogRef<DeleteMemberDialog>,
    private home: HomeServiceService,
    private listcheckSer: ListcheckServiceService,
    private member: MembersService
  ) {}
  onConfirm() {
    this.member.deleteMember(
      this.home.getData().id,
      this.listcheckSer.getMemberDelete()
    );
    this.dialogRef.close();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  templateUrl: "registerCreditDialog.html",
  styleUrls: ["members.component.scss"],
})
export class RegisterCreditDialog {
  rfid: string;
  constructor(
    public dialogRef: MatDialogRef<RegisterCreditDialog>,
    private members: MembersService,
    private user: UserService,
    private listcheckSer: ListcheckServiceService
  ) {}

  onEnter() {
    this.members.registerCard(this.listcheckSer.getMemberRegister(), this.rfid);
    this.dialogRef.close();
  }
  onConfirm() {
    this.dialogRef.close();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  templateUrl: "addMemberDialog.html",
  styleUrls: ["members.component.scss"],
})
export class AddMemberDialog {
  private name = "";
  private username = "";
  private isDisable = false;
  private att = {
    font: "black",
    bg: "green",
    f: "1em",
  };
  private error = "กรุณาเว้นช่องว่าง 1 ช่องระหว่างชื่อและนามสกุล";

  constructor(
    public dialogRef: MatDialogRef<AddMemberDialog>,
    private memberService: MembersService,
    private user: UserService,
    private profile: ProfileService,
    private home: HomeServiceService
  ) {}

  onConfirm() {
    let members = [];
    let arr = [];
    this.name = this.name.trim();
    if (this.username != "" && this.username != this.user.getUser()) {
      if (this.name != "" && this.name.length) {
        let count = [];
        count = this.name.split(" ");
        if (count.length >= 2) {
          this.error = "กรุณาเว้นช่องว่าง 1 ช่องระหว่างชื่อและนามสกุล";
          try {
            this.error = undefined;
            if (this.name.substring(0, 3) === "นาย") {
              this.name = this.name.substr(3);
              var f = this.name.slice(0, this.name.indexOf(" "));
              var l = this.name.slice(
                this.name.indexOf(" ") + 1,
                this.name.length
              );
              arr.push(f, l);
            } else if (this.name.substring(0, 6) === "นางสาว") {
              this.name = this.name.substr(6);
              var f = this.name.slice(0, this.name.indexOf(" "));
              var l = this.name.slice(
                this.name.indexOf(" ") + 1,
                this.name.length
              );
              arr.push(f, l);
            } else {
              var f = this.name.slice(0, this.name.indexOf(" "));
              var l = this.name.slice(
                this.name.indexOf(" ") + 1,
                this.name.length
              );
              arr.push(f, l);
            }

            if (arr.length == 2) {
              members.push({
                username: this.username,
                firstnameTh: arr[0],
                lastnameTh: arr[1].trim(),
              });
            }
          } catch (error) {
            console.log("ERROR NOT TEMPLETE");
          }
        }
      }
    }
    if (this.username == this.user.getUser()) {
      this.error = "ชื่อผู้ใช้ที่ใส่ เป็นเจ้าของห้อง";
    } else if (this.name === "") {
      this.error = "กรุณากรอกข้อมูลให้ถูกต้อง";
    } else {
      if (this.error != undefined) {
        this.error = "กรุณากรอกชื่อและนามสกุลให้ถูกต้อง";
      } else {
        // console.log(this.error);
        this.insertOne(members);
        this.dialogRef.close();
      }
    }
  }
  insertOne(list) {
    if (list.length != 0) {
      // console.log(list);
      var contact: string[] = [list[0].username];
      this.memberService.setContactsList(contact);
      this.memberService.addMemberExcel(list, this.home.getData().id).subscribe(
        (res) => {
          // console.log(res);
        },
        (err) => {
          // console.log(err);
        }
      );
    }
  }
  onClose(): void {
    this.dialogRef.close();
  }
  onKey(event: any) {
    this.username = event.target.value;
    this.profile.hasProfile(this.username).subscribe((res) => {
      if (res != null) {
        this.att.f = "1.3em";
        this.att.font = "DarkGreen";
        this.name = `${res["firstnameTh"]} ${res["lastnameTh"]}`;
        this.isDisable = true;
        this.error = "กรุณาเว้นช่องว่าง 1 ช่องระหว่างชื่อและนามสกุล";
      } else {
        this.name = "";
        this.isDisable = false;
        this.att.f = "1em";
        this.att.font = "black";
      }
    });
  }
}
