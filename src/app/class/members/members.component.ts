import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { MatDialog } from "@angular/material/dialog";
import {
  CodeDialog,
  ImportMembersDialog,
  DeleteMemberDialog,
  RegisterCreditDialog,
  AddMemberDialog,
} from "./members.dialog";
import { ClassService } from "src/app/service/class.service";
import { HomeServiceService } from "src/app/home/home-service.service";
import { MembersService } from "src/app/service/members.service";
import { ListcheckServiceService } from "../listcheck/listcheck-service.service";

export interface UserData {
  id: string;
  username: string;
  name: string;
  status: string;
  start: boolean;
}
export interface member {
  username: string;
  name: string;
}

@Component({
  selector: "app-members",
  templateUrl: "./members.component.html",
  styleUrls: ["./members.component.scss"],
})
export class MembersComponent implements OnInit {
  reportMember: member;
  displayedColumns: string[] = ["id", "username", "name", "status", "tool"];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  userData: UserData[];

  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private member: ClassService,
    private home: HomeServiceService,
    private members: MembersService,
    private listcheckSer: ListcheckServiceService
  ) {}

  ngOnInit() {
    this.createData("onInit");
  }
  createData(pageBefore) {
    if (pageBefore != "addMember" && pageBefore != "importMembers") {
      this.members.getMembers(this.home.getData().id).subscribe(
        (res) => {
          var round = 0;
          this.userData = [];
          // console.log(res)
          for (var x in res) {
            var status;
            if (res[x].status == "สมาชิก") {
              if (res[x].rfid == "") {
                status = "ไม่ได้ลงทะเบียน";
              } else {
                status = "ลงทะเบียนแล้ว";
              }
              this.userData.push({
                id: "",
                username: res[x].username,
                name: res[x].firstnameTh + " " + res[x].lastnameTh,
                status: status,
                start: false,
              });
            }
          }
          myFunction(this.userData);
          for (var x in this.userData) {
            round++;
            this.userData[x].id = round + "";
          }
          this.getTable();
        },
        (err) => {
          console.log("1124");
        }
      );
    } else {
      // console.log(this.members.contactsList);
      this.members.getMembers(this.home.getData().id).subscribe(
        (res) => {
          var round = 0;
          this.userData = [];
          // console.log(res)
          for (var x in res) {
            var status;
            var start;
            if (res[x].status == "สมาชิก") {
              if (res[x].rfid == "") {
                status = "ไม่ได้ลงทะเบียน";
              } else {
                status = "ลงทะเบียนแล้ว";
              }
              for (var i = 0; i < this.members.contactsList.length; i++) {
                if (res[x].username == this.members.contactsList[i]) {
                  start = true;
                  // console.log(res[x].username);
                }
              }
              this.userData.push({
                id: "",
                username: res[x].username,
                name: res[x].firstnameTh + " " + res[x].lastnameTh,
                status: status,
                start: start,
              });
              start = false;
            }
          }
          myFunction(this.userData);
          for (var x in this.userData) {
            round++;
            this.userData[x].id = round + "";
          }
          this.getTable();
        },
        (err) => {
          console.log("1124");
        }
      );
    }
  }

  getTable() {
    this.dataSource = new MatTableDataSource(this.userData);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  codeRoom() {
    const dialogRef = this.dialog.open(CodeDialog, {
      panelClass: "custom-dialog-coderoom",
    });

    dialogRef.afterClosed().subscribe((result) => {
      // console.log("The dialog was closed");
    });
  }

  importMembers() {
    const dialogRef = this.dialog.open(ImportMembersDialog, {
      panelClass: "custom-dialog-import",
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.createData("importMembers");
    });
  }
  deleteMember(row) {
    this.listcheckSer.setMemberDelete(row.username);
    const dialogRef = this.dialog.open(DeleteMemberDialog, {
      panelClass: "custom-dialog-container",
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.createData("deleteMember");
    });
  }
  registerCredit(row) {
    this.listcheckSer.setMemberRegister(row.username);
    const dialogRef = this.dialog.open(RegisterCreditDialog, {
      panelClass: "custom-dialog-container",
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.createData("registerCredit");
    });
  }
  addMember() {
    const dialogRef = this.dialog.open(AddMemberDialog, {
      panelClass: "custom-dialog-add",
    });

    dialogRef.afterClosed().subscribe((result) => {
      // console.log(this.members.contactsList.length)
      this.createData("addMember");
    });
  }

  onClickUpdateReportMember(member) {
    this.reportMember = { username: member.username, name: member.name };
    localStorage.setItem(".reportMember", JSON.stringify(this.reportMember));

    localStorage.setItem(
      ".pathBefore",
      JSON.stringify({ name: "จัดการรายชื่อ" })
    );
  }
}
function myFunction(userData) {
  userData.sort(function (a, b) {
    return a.username - b.username;
  });
}
