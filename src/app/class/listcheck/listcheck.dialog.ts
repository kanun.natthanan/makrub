import { Component, OnInit } from "@angular/core";
import {
  MatDialogRef,
  throwMatDialogContentAlreadyAttachedError,
} from "@angular/material/dialog";
import { formatDate } from "@angular/common";
import { HomeServiceService } from "src/app/home/home-service.service";
import { ListcheckServiceService } from "./listcheck-service.service";
import { ListcheckService } from "src/app/service/listcheck.service";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators,
} from "@angular/forms";
import { ErrorStateMatcher } from "@angular/material/core";

import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from "@angular/material/core";
import * as moment_ from "moment";
const moment = moment_;

export interface States {
  value: string;
}
export interface Lists {
  idpk: string;
  name: string;
  type: string;
  hour: number;
  late: number;
  start: string;
  date: string;
}

@Component({
  templateUrl: "createListcheckDialog.html",
  styleUrls: ["listcheck.component.scss"],
})
export class CreateListcheckDiaolog implements OnInit {
  states: States[] = [
    { value: "บรรยาย" },
    { value: "ปฏิบัติการ" },
    { value: "กิจกรรมอื่น" },
  ];

  private name: string;
  private type: string = "บรรยาย";
  private hour: number = 2;
  private late: number = 15;
  private start: string;
  private date = new Date();
  private list: Lists;
  status: boolean = true;

  nameList: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<CreateListcheckDiaolog>,
    private home: HomeServiceService,
    private listcheck: ListcheckService,
    private formBuilder: FormBuilder,
    private _adapter: DateAdapter<any>
  ) {}

  ngOnInit() {
    this.setNow();
    this.nameList = this.formBuilder.group({
      name: ["", [Validators.required]],
    });
    this._adapter.setLocale("th-TH");
  }

  setNow() {
    let now = new Date();
    let hours = ("0" + now.getHours()).slice(-2);
    let minutes = ("0" + now.getMinutes()).slice(-2);
    let str = hours + ":" + minutes;
    this.start = str;
  }
  applyFilter(filterValue: string, type: string) {
    this.status = true;
    this.listcheck.getListCheck(this.home.getData().id).subscribe(
      (res) => {
        for (var x in res) {
          if (type == "name") {
            if (filterValue == res[x].name && this.type == res[x].type) {
              this.name = "";
              this.status = false;
            }
          } else if (type == "type") {
            if (this.name == res[x].name && filterValue == res[x].type) {
              this.name = "";
              this.status = false;
            }
          }
        }
      },
      (err) => {
        // console.log("1124");
      }
    );
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  onConfirm(): void {
    if (this.name != undefined && this.name != "") {
      this.list = {
        idpk: this.home.getData().id,
        name: this.name,
        type: this.type,
        hour: this.hour,
        late: this.late,
        start: this.start,
        date: moment(this.date).format("DD/MM/YYYY"),
      };
      this.listcheck.addListCheck(this.list);
      this.dialogRef.close();
    }
  }
}

@Component({
  templateUrl: "editListcheckDialog.html",
  styleUrls: ["listcheck.component.scss"],
})
export class EditListcheckDiaolog implements OnInit {
  states: States[] = [
    { value: "บรรยาย" },
    { value: "ปฏิบัติการ" },
    { value: "กิจกรรมอื่น" },
  ];

  private name: string;
  private type: string;
  private hour: number;
  private late: number;
  private date;
  private start: string;
  private list: Lists;
  nameList: FormGroup;
  status: boolean = true;

  constructor(
    public dialogRef: MatDialogRef<EditListcheckDiaolog>,
    private listcheckSer: ListcheckServiceService,
    private listcheck: ListcheckService,
    private formBuilder: FormBuilder,
    private _adapter: DateAdapter<any>,
    private home: HomeServiceService
  ) {}
  ngOnInit() {
    this.setValue();
    this.nameList = this.formBuilder.group({
      name: ["", [Validators.required]],
    });
    this._adapter.setLocale("th-TH");
  }
  setValue() {
    console.log(Number(this.listcheckSer.getData().date.substr(6, 4)) - 543);
    console.log(Number(this.listcheckSer.getData().date.substring(3, 5)));
    console.log(Number(this.listcheckSer.getData().date.substring(0, 2)));
    var dateSet =
      Number(this.listcheckSer.getData().date.substr(6, 4)) -
      543 +
      "-" +
      Number(this.listcheckSer.getData().date.substring(3, 5)) +
      "-" +
      Number(this.listcheckSer.getData().date.substring(0, 2));
    this.name = this.listcheckSer.getData().list;
    this.type = this.listcheckSer.getData().type;
    this.hour = Number(this.listcheckSer.getData().hour);
    this.date = new Date(dateSet);
    this.start = this.listcheckSer.getData().start;
    this.late = Number(this.listcheckSer.getData().late);
  }
  applyFilter(filterValue: string, type: string) {
    this.status = true;
    this.listcheck.getListCheck(this.home.getData().id).subscribe(
      (res) => {
        for (var x in res) {
          if (type == "name") {
            if (filterValue == res[x].name && this.type == res[x].type) {
              this.name = "";
              this.status = false;
            }
          } else if (type == "type") {
            if (this.name == res[x].name && filterValue == res[x].type) {
              this.name = "";
              this.status = false;
            }
          }
        }
      },
      (err) => {
        // console.log("1124");
      }
    );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onConfirm(): void {
    if (this.name == "") {
    } else {
      this.list = {
        idpk: this.listcheckSer.getData().idpk,
        name: this.name,
        type: this.type,
        hour: this.hour,
        late: this.late,
        start: this.start,
        date: moment(this.date).format("DD/MM/YYYY"),
      };
      this.listcheck.updateListCheck(this.list);
      this.dialogRef.close();
    }
  }
}

@Component({
  templateUrl: "deleteListcheckDialog.html",
  styleUrls: ["listcheck.component.scss"],
})
export class DeleteListcheckDialog {
  constructor(
    public dialogRef: MatDialogRef<DeleteListcheckDialog>,
    private homeInService: HomeServiceService,
    private listcheck: ListcheckService,
    private listcheckSer: ListcheckServiceService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  onConfirm() {
    this.listcheck.deleteListCheck(this.listcheckSer.getData().idpk);
    this.dialogRef.close();
  }
}
@Component({
  templateUrl: "./manual-check/checkManualDialog.html",
  styleUrls: ["listcheck.component.scss"],
})
export class CheckManualDialog {
  constructor(
    public dialogRef: MatDialogRef<CheckManualDialog>,
    private homeInService: HomeServiceService,
    private listcheck: ListcheckService,
    private listcheckSer: ListcheckServiceService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  onConfirm() {
    this.dialogRef.close();
  }
}
