import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarcodeCheckComponent } from './barcode-check.component';

describe('BarcodeCheckComponent', () => {
  let component: BarcodeCheckComponent;
  let fixture: ComponentFixture<BarcodeCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarcodeCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarcodeCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
