import { Injectable } from '@angular/core';
import { ListData } from '../listcheck/listcheck.component'


@Injectable({
  providedIn: 'root'
})
export class ListcheckServiceService {
  private data: ListData;
  private deleteMember: string;
  private idList: string;
  private registerMember: string;

  constructor() { }
  setData(data) {
    this.data = {
      id: data.id,
      idpk: data.idpk,
      list: data.list,
      type: data.type,
      hour: data.hour,
      date: data.date,
      start: data.start,
      late: data.late,
    }
    localStorage.setItem(".listSelect",JSON.stringify(this.data))

  }
  getData() {
    let data = localStorage.getItem(".listSelect");
    return JSON.parse(data)
  }
  setMemberDelete(username) {
    this.deleteMember = username
  }
  getMemberDelete() {
    return this.deleteMember
  }
  setIdList(idList){
    this.idList = idList
  }
  getIdList(){
    return this.idList
  }
  setMemberRegister(username){
    this.registerMember = username
  }
  getMemberRegister(){
    return this.registerMember
  }

}
