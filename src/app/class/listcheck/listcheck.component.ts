import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { MatDialog } from "@angular/material";
import {
  CreateListcheckDiaolog,
  EditListcheckDiaolog,
  DeleteListcheckDialog,
} from "./listcheck.dialog";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { HomeServiceService } from "src/app/home/home-service.service";
import { ListcheckService } from "src/app/service/listcheck.service";
import { ListcheckServiceService } from "./listcheck-service.service";

export interface ListData {
  idpk: string;
  id: string;
  list: string;
  type: string;
  hour: string;
  date: string;
  start: string;
  late: string;
}

export interface States {
  value: string;
  viewValue: string;
}

@Component({
  selector: "app-listcheck",
  templateUrl: "./listcheck.component.html",
  styleUrls: ["./listcheck.component.scss"],
})
export class ListcheckComponent implements OnInit {
  states: States[] = [
    { value: "ทั้งหมด", viewValue: "" },
    { value: "บรรยาย", viewValue: "บรรยาย" },
    { value: "ปฏิบัติการ", viewValue: "ปฏิบัติการ" },
    { value: "กิจกรรมอื่น", viewValue: "กิจกรรมอื่น" },
  ];
  sumUser = 0;

  listData: ListData[];
  editData: ListData;

  displayedColumns: string[] = [
    "id",
    "list",
    "type",
    "hour",
    "date",
    "check",
    "tool",
  ];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  dataSource: MatTableDataSource<ListData>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private home: HomeServiceService,
    private listcheck: ListcheckService,
    private listcheckSer: ListcheckServiceService
  ) {}

  ngOnInit() {
    this.createData();
  }
  createData() {
    this.listcheck.getListCheck(this.home.getData().id).subscribe(
      (res) => {
        var round = 0;
        this.listData = [];
        // console.log(res)
        for (var x in res) {
          round++;
          var date =
            res[x].date.toString().substring(0, 6) +
            (Number(res[x].date.toString().substr(6, 4)) + 543);
          this.listData.push({
            idpk: res[x]._id,
            id: round + "",
            list: res[x].name,
            type: res[x].type,
            hour: res[x].hour,
            date: date,
            start: res[x].start,
            late: res[x].late,
          });
        }
        this.getTable();
      },
      (err) => {
        // console.log("1124");
      }
    );
  }
  getTable() {
    this.dataSource = new MatTableDataSource(this.listData);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  createListcheck() {
    const dialogRef = this.dialog.open(CreateListcheckDiaolog, {
      panelClass: "custom-dialog-createList",
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.createData();
    });
  }
  setData(row) {
    this.listcheckSer.setData(row);
  }
  editListcheck(row) {
    this.listcheckSer.setData(row);
    const dialogRef = this.dialog.open(EditListcheckDiaolog, {
      panelClass: "custom-dialog-createList",
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.createData();
    });
  }
  deleteListcheck(row) {
    this.listcheckSer.setData(row);
    const dialogRef = this.dialog.open(DeleteListcheckDialog, {
      panelClass: "custom-dialog-container",
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.createData();
    });
  }
}
