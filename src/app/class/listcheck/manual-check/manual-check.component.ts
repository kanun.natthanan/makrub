import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material'
import { MembersService } from 'src/app/service/members.service';
import { HomeService } from 'src/app/service/home.service';
import { HomeServiceService } from 'src/app/home/home-service.service';
import { ListcheckServiceService } from '../listcheck-service.service';
import { ListcheckService } from 'src/app/service/listcheck.service';
import { CheckManualDialog } from '../listcheck.dialog';


export interface UserData {
  id: number;
  username: string;
  name: string;
  status: string;
}
export interface ManualCheck {
  username: string;
  status: string;
}


@Component({
  selector: 'app-manual-check',
  templateUrl: './manual-check.component.html',
  styleUrls: ['./manual-check.component.scss']
})
export class ManualCheckComponent implements OnInit {
  userData: UserData[];


  displayedColumns: string[] = ['id', 'username', 'name', 'onTime', 'late', 'leave', 'sick', 'notSendLeave'];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;


  constructor(public dialog: MatDialog,
    private home: HomeServiceService,
    private members: MembersService,
    private listCheckSer: ListcheckServiceService,
    private listCheck: ListcheckService,
  ) {


  }

  ngOnInit() {
    this.createTable()

  }
  createTable() {
    
    this.members.getMemberManual(this.listCheckSer.getData().idpk).subscribe(
      res => {
        var round = 0;
        this.userData = [];
        // console.log(res)
        for (var x in res) {
          this.userData.push({
            id: 0,
            username: res[x].username,
            name: res[x].firstnameTh + " " + res[x].lastnameTh,
            status: res[x].status,
          });

        }
        myFunction(this.userData)
        for (var x in this.userData) {
          round++;
          this.userData[x].id = round;
        }

        this.getTable()
      },
      err => {
        console.log("CREATE TABLE ERROR");

      }
    );
  }
  getTable() {
    this.dataSource = new MatTableDataSource(this.userData);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  setValue(event, id, username) {
    this.userData[id - 1].status = event;
    this.checkManual()
  }

  getValue() {
    this.listCheck.updateManuakCheck(this.listCheckSer.getData().idpk, this.userData)
    const dialogRef = this.dialog.open(CheckManualDialog, {
      panelClass: "custom-dialog-container"
  });

  dialogRef.afterClosed().subscribe(result => {
    
  });
   
  }

  checkManual(){
    this.listCheck.updateManuakCheck(this.listCheckSer.getData().idpk, this.userData)
  }


}
function myFunction(userData) {
  userData.sort(function (a, b) { return a.username - b.username });
}






