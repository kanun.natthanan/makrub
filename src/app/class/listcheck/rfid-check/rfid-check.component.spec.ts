import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfidCheckComponent } from './rfid-check.component';

describe('RfidCheckComponent', () => {
  let component: RfidCheckComponent;
  let fixture: ComponentFixture<RfidCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfidCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfidCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
