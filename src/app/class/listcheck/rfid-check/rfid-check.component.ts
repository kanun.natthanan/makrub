import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { MatDialog } from "@angular/material";
import { ListcheckService } from "src/app/service/listcheck.service";
import { ListcheckServiceService } from "../listcheck-service.service";
import { MembersService } from "src/app/service/members.service";

export interface UserData {
  id: number;
  username: string;
  name: string;
  status: string;
}

@Component({
  selector: "app-rfid-check",
  templateUrl: "./rfid-check.component.html",
  styleUrls: ["./rfid-check.component.scss"],
})
export class RfidCheckComponent implements OnInit, OnDestroy {
  users: UserData[] = [];
  name = "";
  usernameShow = "";
  username = "";
  private clock = "00:00:00";
  private late = 0;
  private now = 0;
  private timecolor = "black";
  private imgCheck = "../../../../assets/img/people.png";
  private myClock;

  displayedColumns: string[] = ["id", "username", "name", "status"];
  columnsToDisplay: string[] = this.displayedColumns.slice();

  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private listCheck: ListcheckService,
    private list: ListcheckServiceService,
    private members: MembersService
  ) {}

  ngOnInit() {
    this.createTable();
    this.myClock = setInterval(() => {
      this.clock = new Date().toLocaleTimeString("it-IT");
      this.getLate();
      if (this.late - this.now < 0) {
        this.timecolor = "red";
      }
    }, 1000);
  }
  ngOnDestroy() {
    clearTimeout(this.myClock);
  }
  createTable() {
    this.members.getMemberManual(this.list.getData().idpk).subscribe(
      (res) => {
        this.users = [];
        // console.log(res)
        for (var x in res) {
          var status;
          if (res[x].status != "empty") {
            if (res[x].status == "onTime") {
              status = "ทันเวลา";
            } else if (res[x].status == "late") {
              status = "สาย";
            } else if (res[x].status == "leave") {
              status = "ลากิจ";
            } else if (res[x].status == "sick") {
              status = "ลาป่วย";
            } else if (res[x].status == "notSendLeave") {
              status = "ไม่ส่งใบลา";
            }
            this.users.push({
              id: 0,
              username: res[x].username,
              name: res[x].firstnameTh + " " + res[x].lastnameTh,
              status: status,
            });
          }
        }
        myFunction(this.users);
        var round = 0;
        for (var x in this.users) {
          round++;
          this.users[x].id = round;
        }

        this.getTable();
      },
      (err) => {
        console.log("CREATE TABLE ERROR");
      }
    );
  }
  getTable() {
    myFunctionSwitch(this.users);
    this.dataSource = new MatTableDataSource(this.users);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  onEnter() {
    this.listCheck.getMember(this.username).subscribe(
      (res) => {
        if (res["status"] != null) {
          this.username = res["username"];
        }
        this.onCheck();
      },
      (err) => {
        console.log("UPDATE ERROR");
      }
    );
  }
  onCheck() {
    this.usernameShow = this.username;
    var check = {
      idList: this.list.getData().idpk,
      member: [
        {
          id: "55",
          username: this.username,
          name: "sss",
          status: "empty",
        },
      ],
    };
    this.listCheck.isMember(this.list.getData().idpk, this.username).subscribe(
      (res) => {
        let has = true;
        for (let x in res) {
          if (this.username === res[x].username) {
            has = false;
            // console.log(res[x]);
            this.listCheck.getImg(this.username).subscribe((res) => {
              // console.log(res["img"]);
              if (res["img"].length == 0) {
                this.imgCheck = "../../assets/img/people.png";
              } else {
                this.imgCheck =
                  "http://thebeach.buu.in.th:4011/images/" + res["img"];
              }
              this.name = `${res["firstnameTh"]} ${res["lastnameTh"]}`;

              this.listCheck
                .getDetailList(this.list.getData().idpk)
                .subscribe((res) => {
                  var d = new Date();
                  var late =
                    parseInt(
                      res["start"].slice(
                        0,
                        parseInt(res["start"].indexOf(":") + "")
                      )
                    ) *
                      60 +
                    parseInt(
                      res["start"].slice(
                        parseInt(res["start"].indexOf(":") + 1 + ""),
                        res["start"].length
                      )
                    ) +
                    parseInt(res["late"] + "");
                  let h = parseInt(this.clock.substr(0, 2));
                  let m = parseInt(this.clock.substr(3, 2));
                  this.now = h * 60 + m;
                  // console.log(late - now);
                  if (this.late - this.now >= 0 && this.late - this.now < 25) {
                    check.member[0].status = "onTime";
                  } else {
                    check.member[0].status = "late";
                  }
                  check.member[0].name = this.name;
                  this.listCheck.updateBarcodeCheck(check).subscribe(
                    (res) => {
                      // console.log(res);
                      var status;
                      if (check.member[0].status == "onTime") {
                        status = "ทันเวลา";
                      } else if (check.member[0].status == "late") {
                        status = "สาย";
                      }
                      this.users.forEach((element, index) => {
                        if (element["username"] === check.member[0].username) {
                          // console.log(element.username);
                          this.users.forEach((element2) => {
                            // console.log(element2);
                            if (
                              element2.id >= element.id &&
                              element["username"] != element2.username
                            ) {
                              element2.id--;
                            }
                          });
                          element.id = this.users.length;
                        }
                      });
                      if (this.users.length != 0) {
                        let has = true;
                        for (x in this.users) {
                          if (
                            check.member[0].username === this.users[x].username
                          ) {
                            has = false;
                            break;
                          }
                        }
                        if (has) {
                          this.users.push({
                            id: this.users.length + 1,
                            username: check.member[0].username,
                            name: check.member[0].name,
                            status: status,
                          });
                        }
                      } else {
                        this.users.push({
                          id: this.users.length + 1,
                          username: check.member[0].username,
                          name: check.member[0].name,
                          status: status,
                        });
                      }
                      this.getTable();
                    },
                    (err) => {
                      console.log("ERROR UPDATE");
                    }
                  );
                });
            });
            break;
          } else {
            // console.log("NO MEMBER");
          }
        }
        if ((has = true)) {
          this.imgCheck = "../../../../assets/img/people.png";
          this.username = "";
          this.name = "ไม่พบข้อมูล";
        }
        this.username = "";
      },
      (err) => {
        console.log("ERROR MEMBER");
      }
    );
  }
  getLate() {
    this.listCheck.getDetailList(this.list.getData().idpk).subscribe(
      (res) => {
        this.late =
          parseInt(
            res["start"].slice(0, parseInt(res["start"].indexOf(":") + ""))
          ) *
            60 +
          parseInt(
            res["start"].slice(
              parseInt(res["start"].indexOf(":") + 1 + ""),
              res["start"].length
            )
          ) +
          parseInt(res["late"] + "");
        let h = parseInt(this.clock.substr(0, 2));
        let m = parseInt(this.clock.substr(3, 2));
        this.now = h * 60 + m;
      },
      (err) => {
        console.log("ERROR MEMBER");
      }
    );
  }
}
function myFunction(userData) {
  userData.sort(function (a, b) {
    return a.username - b.username;
  });
}
function myFunctionSwitch(userData) {
  userData.sort(function (a, b) {
    return b.id - a.id;
  });
}
