import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "./material/material.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LoginComponent } from "./login/login.component";
import { HomeComponent } from "./home/home.component";
import { ProfileComponent } from "./profile/profile.component";
import { PersonalComponent } from "./profile/personal/personal.component";
import { PasswordComponent } from "./profile/password/password.component";
import { HomeCardComponent } from "./home/home-card/home-card.component";
import { ListcheckComponent } from "./class/listcheck/listcheck.component";
import { ClassComponent } from "./class/class.component";
import { ManualCheckComponent } from "./class/listcheck/manual-check/manual-check.component";
import { MembersComponent } from "./class/members/members.component";
import { ReportComponent } from "./class/report/report.component";
import { ReportListComponent } from "./class/report/report-list/report-list.component";
import { ReportResultComponent } from "./report-result/report-result.component";
import { ReportMembersComponent } from "./class/report-members/report-members.component";
import { BarcodeCheckComponent } from "./class/listcheck/barcode-check/barcode-check.component";
import { ClassMemberComponent } from "./class-member/class-member.component";
import { StoreComponent } from "./store/store.component";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { RfidCheckComponent } from './class/listcheck/rfid-check/rfid-check.component'

////DIALOG
import { DeleteDialog, EditDialog, AddDialog,JoinClassDialog } from "./home/home.dialog";
import { RestoreDialog, LostDialog } from "./store/storeDialog";
import {
  CreateListcheckDiaolog,
  EditListcheckDiaolog,
  DeleteListcheckDialog
} from "./class/listcheck/listcheck.dialog";
import {
  CodeDialog,
  ImportMembersDialog,
  DeleteMemberDialog,
  RegisterCreditDialog,
  AddMemberDialog,
} from "./class/members/members.dialog";
import { CheckManualDialog } from "./class/listcheck/listcheck.dialog";
import { UpdateProfileDialog } from "./profile/personal/personal.dialog";
///////////SERVICE
import { UserService } from "./service/user.service";
import { AuthInterceptor } from "./auth/auth.interceptor.service";
import { HomeService } from "./service/home.service";
import { ClassService } from "./service/class.service";
import { ProfileService } from "./service/profile.service";
import { ReportResultService } from "./service/report-result.service";
import { ListcheckService } from "./service/listcheck.service";
import { MembersService } from "./service/members.service";
import { ListcheckServiceService } from "./class/listcheck/listcheck-service.service";
import { ReportService } from "./service/report.service";

import { MatInputModule, MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ProfileComponent,
    PersonalComponent,
    PasswordComponent,
    HomeCardComponent,
    ListcheckComponent,
    ClassComponent,
    DeleteDialog,
    EditDialog,
    JoinClassDialog,
    AddDialog,
    ManualCheckComponent,
    MembersComponent,
    ReportComponent,
    ReportListComponent,
    ReportResultComponent,
    CreateListcheckDiaolog,
    ReportMembersComponent,
    BarcodeCheckComponent,
    CodeDialog,
    ImportMembersDialog,
    EditListcheckDiaolog,
    ClassMemberComponent,
    UpdateProfileDialog,
    StoreComponent,
    DeleteListcheckDialog,
    DeleteMemberDialog,
    RestoreDialog,
    LostDialog,
    CheckManualDialog,
    RegisterCreditDialog,
    RfidCheckComponent,
    AddMemberDialog,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  exports: [
    MatDatepickerModule,
    MatNativeDateModule
  ],
  providers: [
    UserService,
    HomeComponent,
    HomeService,
    ClassService,
    ProfileService,
    ReportResultService,
    ListcheckService,
    MembersService,
    ListcheckServiceService,
    ReportService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {provide: MAT_DATE_LOCALE, useValue: 'th-TH'},
  ],
  entryComponents: [
    DeleteDialog,
    EditDialog,
    AddDialog,
    JoinClassDialog,
    CreateListcheckDiaolog,
    CodeDialog,
    ImportMembersDialog,
    EditListcheckDiaolog,
    UpdateProfileDialog,
    DeleteListcheckDialog,
    RestoreDialog,
    LostDialog,
    DeleteMemberDialog,
    CheckManualDialog,
    RegisterCreditDialog,
    AddMemberDialog
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
