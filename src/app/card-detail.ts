export interface Subject {
    group : String,
    status : String,
    pic_id : String,
    subject_name : String,
    idpk : String, //unique
    detail : String,
    code : String,
    sort : number,
    store : String
  }