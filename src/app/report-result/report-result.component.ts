import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { ProfileService } from "../service/profile.service";
import { UserService } from "../service/user.service";
import { Router } from "@angular/router";
import { HomeServiceService } from "../home/home-service.service";
import { ReportResultService } from "../service/report-result.service";
import { MembersService } from "../service/members.service";
import { Subject } from "../card-detail";

export interface UserData {
  id: number;
  username: string;
  name: string;
  statusCheck: listCheck[];
}

export interface listCheck {
  name: string;
  status: string;
}

export interface ClassRoom {
  name: string;
  group: string;
  detail: string;
}

@Component({
  selector: "app-report-result",
  templateUrl: "./report-result.component.html",
  styleUrls: ["./report-result.component.scss"]
})
export class ReportResultComponent implements OnInit {
  opened = false;
  classRoom: ClassRoom = { name: "", detail: "", group: "" };
  userData: UserData[] = [];

  imageUrl: string = "";
  private selectSubject;
  private subjects_new;

  displayedColumns: string[] = [];
  columnsToDisplay: string[] = [];
  type: string[] = [];
  listcheck: listCheck[] = [];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private profile: ProfileService,
    private user: UserService,
    private router: Router,
    private home: HomeServiceService,
    private report: ReportResultService,
    private member: MembersService
  ) {}

  ngOnInit() {
    this.subjects_new = this.home.getSubjects();
    this.classRoom.name = this.home.getData().name;
    this.classRoom.detail = this.home.getData().detail;
    this.classRoom.group = this.home.getData().group;
    this.createTable();

    this.profile.getImage(this.user.getUser()).subscribe(
      (res) => {
        if (res["img"].length == 0) {
          this.imageUrl = "../../assets/img/people.png";
        } else {
          this.imageUrl = "http://thebeach.buu.in.th:4011/images/" + res["img"];
        }
      },
      (err) => {
        console.log("IMG ERROR")
      }
    );
  }
  createTable() {
    this.member.getMembers(this.home.getData().id).subscribe(
      res => {
        // console.log(res);
        var round = 0;
        for (var x in res) {
          if (res[x].status != "เจ้าของห้อง") {
            this.userData.push({
              id: round,
              username: res[x].username,
              name: res[x].firstnameTh + " " + res[x].lastnameTh,
              statusCheck: []
            });
          }
        }
        myFunction(this.userData);
        for (var x in this.userData) {
          round++;
          this.userData[x].id = round;
        }
      },
      err => {
        console.log("CREATE TABLE ERROR");
      }
    );
    this.report.getReportAll(this.home.getData().id).subscribe(
      res => {
        this.displayedColumns = ["id", "username", "name"];
        this.type = ["", "", ""];
        for (var x in res) {
          var typeNum;
          if(res[x].type == "บรรยาย"){
            this.displayedColumns.push(res[x].name+"0");
            
            typeNum = "0";
          }else if(res[x].type == "ปฏิบัติการ"){
            this.displayedColumns.push(res[x].name+"1");
            typeNum = "1";
          }else if(res[x].type == "กิจกรรมอื่น"){
            this.displayedColumns.push(res[x].name+"2");
            typeNum = "2";
          }
          this.type.push(res[x].type);
          for (y in this.userData) {
            this.userData[y].statusCheck.push({
              name: res[x].name+typeNum,
              status: "ไม่ได้ลงทะเบียน"
            });
          }
        }
        for (var x in res) {
          var typeNum;
          if(res[x].type == "บรรยาย"){
            typeNum = "0";
          }else if(res[x].type == "ปฏิบัติการ"){
            typeNum = "1";
          }else if(res[x].type == "กิจกรรมอื่น"){
            typeNum = "2";
          }
          for (var y in res[x].member) {
            for (var k in this.userData) {
              for (var j in this.userData[k].statusCheck) {
                console.log(res[x].name+typeNum)
                if (
                  res[x].member[y].username == this.userData[k].username &&
                  this.userData[k].statusCheck[j].name == res[x].name+typeNum
                ) {
                  if (res[x].member[y].status == "onTime") {
                    this.userData[k].statusCheck[j].status = "ทันเวลา";
                  } else if (res[x].member[y].status == "late") {
                    this.userData[k].statusCheck[j].status = "สาย";
                  } else if (res[x].member[y].status == "leave") {
                    this.userData[k].statusCheck[j].status = "ลากิจ";
                  } else if (res[x].member[y].status == "sick") {
                    this.userData[k].statusCheck[j].status = "ลาป่วย";
                  } else if (res[x].member[y].status == "notSendLeave") {
                    this.userData[k].statusCheck[j].status = "ไม่ส่งใบลา";
                  }
                }
              }
            }
          }
        }
        console.log(this.userData);
        this.columnsToDisplay = this.displayedColumns.slice();
        this.getTable();
      },
      err => {
        console.log("CREATE TABLE ERROR");
      }
    );
  }
  getTable() {
    this.dataSource = new MatTableDataSource(this.userData);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  get(element) {
    // console.log(element);
  }
  onLogout() {
    localStorage.clear();
    this.router.navigate(["/"]);
  }
  onProfile() {
    this.router.navigate(["/profile"]);
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onGoTo(sub: Subject): void {
    this.selectSubject = sub;
    // console.log(this.selectSubject);
    this.home.setSubject(this.selectSubject);
    if (this.home.getAction() === undefined) {
      this.home.toChaeckList(this.selectSubject);
      // console.log(this.selectSubject.status);
      if (this.selectSubject.status === "เจ้าของห้อง") {
        this.router.navigate(["/listCheck"]);
      } else {
        this.router.navigate(["/classMember"]);
      }
    }
    this.home.setAction(undefined);
  }
}
function myFunction(userData) {
  userData.sort(function(a, b) {
    return a.username - b.username;
  });
}
