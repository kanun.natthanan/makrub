import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserService } from "../service/user.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  user = {
    username: "",
    password: ""
  };
  loginStatus = true;

  constructor(private router: Router, private userService: UserService) {}

  ngOnInit() {}
  onLogin() {
    this.userService.login(this.user).subscribe(
      res => {
        // console.log(res);
        this.userService.setToken(res['token'])
        this.router.navigate(["/home"]);
      },
      err => {
        this.loginStatus = false
        // console.log(this.user);
      }
    );
    this.userService.setUser(this.user.username)
   
  }
}
