import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { ReportResultComponent } from './report-result/report-result.component'
import { ListcheckComponent } from './class/listcheck/listcheck.component';
import { MembersComponent } from './class/members/members.component';
import { ReportComponent } from './class/report/report.component';
import { ManualCheckComponent } from './class/listcheck/manual-check/manual-check.component';
import { BarcodeCheckComponent } from './class/listcheck/barcode-check/barcode-check.component';
import { ReportListComponent } from './class/report/report-list/report-list.component';
import { ReportMembersComponent } from './class/report-members/report-members.component'
import { ClassMemberComponent } from './class-member/class-member.component';
import { StoreComponent } from './store/store.component';
import { RfidCheckComponent } from './class/listcheck/rfid-check/rfid-check.component'


const routes: Routes = [
  {path: '', redirectTo: 'login',
  pathMatch: 'full'},
  {path: 'login',component:LoginComponent},
  {path: 'home',component:HomeComponent},
  {path: 'profile',component:ProfileComponent},
  {path: 'reportResult',component:ReportResultComponent},
  {path: 'listCheck', component:ListcheckComponent},
  {path: 'members',component:MembersComponent},
  {path: 'report',component:ReportComponent},
  {path: 'manualCheck',component:ManualCheckComponent},
  {path: 'barcodeCheck',component:BarcodeCheckComponent},
  {path: 'reportList',component:ReportListComponent},
  {path: 'reportMember',component:ReportMembersComponent},
  {path: 'classMember',component:ClassMemberComponent},
  {path: 'homeStore',component:StoreComponent},
  {path: 'rfidCheck',component:RfidCheckComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
